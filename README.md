# Game of Kono

This is a board game in which the user plays against an AI.

I am including the class definition (java files) and the object design (xml files) source code. You have to create a project and then migrate folders and files to the main folder in your project.

You can dowload this app by going to the Google Play Store: [Game of Kono](https://play.google.com/store/apps/details?id=com.egkono.kono)

Any feedback or comments are greatly appreciated.

Thanks,

Ed Gar 
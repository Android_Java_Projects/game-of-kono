package com.egkono.kono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class extends the Player class
 It contains additional properties and methods
 that only apply to the Computer player
 **********************************************************/
public class PlayerCPU extends Player {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Constants that define a strategy
    private static final int CAPTURE_PIECE = 0;
    private static final int CAPTURE_GOAL = 1;
    private static final int BLOCK_OPPONENT = 2;
    private static final int MOVE_FORWARD = 3;
    private static final int MOVE_BACKWARD = 4;

    // Contains all possible moves
    private ArrayList<AIPossibleMoves> m_possibleMoves;

    // Best move to perform
    private AIPossibleMoves m_bestMove;

    // Message to user
    private String m_messageToUser;

    /**
     * Class constructor
     */
    public PlayerCPU() {
        class_Initializer();
    }


    /* *********************************************************************
    Function Name : counterMove
    Purpose : Calculates the best possible counter move to perform
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates the best possible counter move to perform
     * @param a_konoBoard The current kono board
     */
    public void counterMove(Board a_konoBoard){

        // Compute all possible moves
        computeAllPossibleMoves(a_konoBoard,getM_pieceColor(),getM_goalOrientation());

        // Analyze all possible moves
        analyzeAllPossibleMoves(a_konoBoard, getM_goalLocationsScoreUser());

        // Pick the best move
        pickBestPossibleMove();

        // Make the move
        makeBestPossibleMove(a_konoBoard);

        // Change/Update piece/location moved
        updatePieceMoved(a_konoBoard);

    }


    /* *********************************************************************
    Function Name : userHelp
    Purpose : Calculates the best possible move for the user when the user asks for help
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates the best possible move for the user when the user asks for help
     * @param a_konoBoard The current kono board
     * @param a_pieceColor Piece color to calculate all the moves
     * @param a_goalOrientation Winning goal orientation
     * @param a_goalLocationScore All goal locations and their respective points per position
     */
    public void userHelp(Board a_konoBoard, String a_pieceColor, String a_goalOrientation, HashMap<String, Integer> a_goalLocationScore){

        // Compute all possible moves
        computeAllPossibleMoves(a_konoBoard,a_pieceColor,a_goalOrientation);

        // Analyze all possible moves
        analyzeAllPossibleMoves(a_konoBoard, a_goalLocationScore);

        // Pick the best move
        pickBestPossibleMove();

        // Save message into a system log
        SystemFeedback.setM_feedback(getStrategyDescription(m_bestMove, "user"));

        // Display message to the user
        setM_messageToUser(getStrategyDescription(m_bestMove, "user"));
    }


    /* *********************************************************************
    Function Name : getM_bestMove
    Purpose : Returns the best move possible.
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the best move possible
     * @return AIPossibleMoves object representing the best move possible
     */
    public final AIPossibleMoves getM_bestMove(){
        return m_bestMove;
    }


    /* *********************************************************************
    Function Name : computeAllPossibleMoves
    Purpose : Computes all possible moves.  This function is also used to help the user.
    Parameters : See below
    Return Value : void
    Local Variables :
        boardSize, size of the current kono board
    Algorithm :
        1) Get the size of the current kono board
        2) Clear m_possibleMoves ArrayList
        3) For every player's piece, check if a northeast, northwest, southeast, and/or southwest
           move is possible.
        4) Append every possible move to the m_possibleMoves ArrayList
    Assistance Received : none
    ********************************************************************* */
    /**
     * Computes all possible moves.
     * @param a_konoBoard The current kono board
     * @param a_pieceColor Piece color to calculate all the moves
     * @param a_goalOrientation Winning goal orientation
     */
    private void computeAllPossibleMoves(Board a_konoBoard, String a_pieceColor, String a_goalOrientation){
        // Get the size of the current kono board
        int boardSize = a_konoBoard.getM_boardSize();

        // Clear all possible moves arrayList
        m_possibleMoves.clear();

        // Loop thru board locations
        for (int row = 0; row < boardSize; row++){
            for (int col = 0; col < boardSize; col++){
                // Check if the piece to calculate move is the player's piece
                if (a_konoBoard.getSingleContent(row,col).substring(0,1).equals(a_pieceColor)){

                    // If any of the following movement is possible push it onto the ArrayList of m_possibleMoves
                    // First possible move - northeast
                    if (a_konoBoard.isMoveValid(row,col,row-1,col+1)){
                        buildPossibleMove(row,col,row-1, col+1,a_konoBoard.getSingleContent(row,col),
                                a_konoBoard.getSingleContent(row-1,col+1), a_goalOrientation);
                    }

                    // Second possible move - northwest
                    if (a_konoBoard.isMoveValid(row,col,row-1,col-1)){
                        buildPossibleMove(row,col,row-1, col-1,a_konoBoard.getSingleContent(row,col),
                                a_konoBoard.getSingleContent(row-1,col-1), a_goalOrientation);
                    }

                    // Third possible move - southeast
                    if (a_konoBoard.isMoveValid(row,col,row+1,col+1)){
                        buildPossibleMove(row,col,row+1, col+1,a_konoBoard.getSingleContent(row,col),
                                a_konoBoard.getSingleContent(row+1,col+1), a_goalOrientation);
                    }

                    // Fourth possible move - southwest
                    if (a_konoBoard.isMoveValid(row,col,row+1,col-1)){
                        buildPossibleMove(row,col,row+1, col-1,a_konoBoard.getSingleContent(row,col),
                                a_konoBoard.getSingleContent(row+1,col-1), a_goalOrientation);
                    }
                }
            }
        }
    }


    /* *********************************************************************
    Function Name : buildPossibleMove
    Purpose : Appends a possible move to the array of possible moves
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Appends a possible move to the array of possible moves
     * @param a_rowOrigin Origin row location
     * @param a_colOrigin Origin column location
     * @param a_rowDestination Destination row location
     * @param a_colDestination Destination column location
     * @param a_contentsOrigin Origin contents
     * @param a_contentsDestination Destination contents
     * @param a_goalOrientation Winning goal orientation
     */
    private void buildPossibleMove(int a_rowOrigin, int a_colOrigin, int a_rowDestination, int a_colDestination,
                                   String a_contentsOrigin, String a_contentsDestination, String a_goalOrientation){
        AIPossibleMoves possibleMove = new AIPossibleMoves(a_rowOrigin,a_colOrigin,a_rowDestination, a_colDestination,
                a_contentsOrigin,a_contentsDestination, a_goalOrientation);
        m_possibleMoves.add(possibleMove);
    }


    /* *********************************************************************
    Function Name : analyzeAllPossibleMoves
    Purpose : Analyzes all possible moves
    Parameters : See below
    Return Value : void
    Local Variables :
        priority, a priority given to a possible move
    Algorithm :
        1) For every possible move:
           - If the origin contents is a super piece, than it is capturing a piece.
           - Check if the next move is a goal location, and the current location is not a goal already
           - Check if a piece can block an opponent
           - Check if the piece can move forward
           - If the piece is not moving forward then it is moving backwards
           - If it is moving backwards check if the current position is in a goal, if so, give it a lower priority
        2) For every possible move update the potential points to be gained and the priority according to the
           constants that define a strategy
    Assistance Received : none
    ********************************************************************* */
    /**
     * Analyzes all possible moves
     * @param a_konoBoard The current kono board
     * @param a_goalLocationScore All goal locations and their respective points per position
     */
    private void analyzeAllPossibleMoves(Board a_konoBoard, HashMap<String,Integer> a_goalLocationScore){
        int priority;

        for (AIPossibleMoves move : m_possibleMoves) {

            // If the origin contents is a super piece, than it is capturing a piece
            if (move.getM_contentsOrigin().equals("WW") && move.getM_contentsDestination().substring(0,1).equals("B") ||
               (move.getM_contentsOrigin().equals("BB") && move.getM_contentsDestination().substring(0,1).equals("W") )){
                updatePossibleMove(move,5,15,
                        CAPTURE_PIECE);
                continue;
            }

            // Check if the next move is a goal location, and the current location is not a goal already
            if (    !isGoalLocation(move.getM_rowOrigin(),move.getM_colOrigin(), a_goalLocationScore)  &&
                    isGoalLocation(move.getM_rowDestination(), move.getM_colDestination(), a_goalLocationScore)) {
                updatePossibleMove(move,getPointsInGoalLocation(move.getM_rowDestination(), move.getM_colDestination(),
                        a_goalLocationScore),getPointsInGoalLocation(move.getM_rowDestination(), move.getM_colDestination(),
                        a_goalLocationScore)+3,
                        CAPTURE_GOAL);
                continue;
            }

            // Check if a piece can block an opponent
            if (isPieceBlocking(move,a_konoBoard)) {
                updatePossibleMove(move,0,1,
                        BLOCK_OPPONENT);
                continue;
            }

            // Check if the piece can move forward
            if (computeOrientation(move.getM_rowOrigin(), move.getM_rowDestination()).equals(move.getM_goalOrientation())) {
                updatePossibleMove(move,0,0,
                        MOVE_FORWARD);
                continue;
            }

            // If the piece is not moving forward then it is moving backwards
            priority = -1;

            // If it is moving backwards check if the current position is in a goal, give it a lower priority
            if ( isGoalLocation(move.getM_rowOrigin(),move.getM_colOrigin(), a_goalLocationScore) ) priority = -2;
            updatePossibleMove(move,0,priority,
                    MOVE_BACKWARD);
        }
    }


    /* *********************************************************************
    Function Name : isPieceBlocking
    Purpose : Checks if the destination position can block an opponent
    Parameters : See below
    Return Value : See below
    Local Variables :
        opponent, opponent's piece color
        tempRow, temporal row location
        tempCol1, temporal right column location
        tempCol2, temporal left columns location
        flag, true if a piece is the location indicated tempRow and tempCols
        pieceInLocation, contents in the location indicated by tempRow and tempCols
    Algorithm :
        1) Check if the blocking procedure can be performed while moving towards the goal
           - If the goal orientation is (S)outh, ask if the destination row is greater than the origin row
           - If the goal orientation is (N)orth, ask if the destination row is less than the origin row
        2) Calculate the opponent
        3) One piece can only block 2 positions at a time, opponents are blocked according to the goal heading
        4) Try to get contents by the estimated temporal locations
        5) Return boolean which indicates if a piece can block an opponent's piece
    Assistance Received : none
    ********************************************************************* */
    /**
     * Checks if the destination position can block an opponent
     * @param a_move A possible move
     * @param a_konoBoard The current kono board
     * @return True if the destination is blocking the opponent
     */
    private boolean isPieceBlocking(AIPossibleMoves a_move, Board a_konoBoard ){
        // Check if the blocking procedure can be performed while moving towards the goal
        // If the goal orientation is (S)outh, ask if the destination row is greater than the origin row
        // If the goal orientation is (N)orth, ask if the destination row is less than the origin row
        if (a_move.getM_goalOrientation().equals("S") && !(a_move.getM_rowDestination() > a_move.getM_rowOrigin())) return false;
        else if (a_move.getM_goalOrientation().equals("N") && !(a_move.getM_rowDestination() < a_move.getM_rowOrigin())) return false;

        // Calculate the opponent
        String opponent = (a_move.getM_contentsOrigin().substring(0,1).equals("W")) ? "B" : "W";

        // One piece can only block 2 positions at a time
        // Opponents are blocked according to the goal heading
        int tempRow;
        int tempCol1 = a_move.getM_colDestination() + 1;
        int tempCol2 = a_move.getM_colDestination() - 1;

        if (a_move.getM_goalOrientation().equals("S")) tempRow = a_move.getM_rowDestination() + 1;
        else tempRow = a_move.getM_rowDestination() - 1;

        // Position 1
        // Try to get the contents using the temp variables
        boolean flag;
        String pieceInLocation = "none";
        try {
            pieceInLocation = a_konoBoard.getSingleContent(tempRow, tempCol1);
            flag = true;
        }
        catch (Exception e) {
            // The content is out of bounds
            flag = false;
        }

        if (flag && pieceInLocation.equals(opponent)) return true;

        // Position 2
        // Try to get the contents using the temp variables
        try {
            pieceInLocation = a_konoBoard.getSingleContent(tempRow, tempCol2);
            flag = true;
        }
        catch (Exception e) {
            // The content is out of bounds
            flag = false;
        }

        return flag && pieceInLocation.equals(opponent);
    }


    /* *********************************************************************
    Function Name : updatePossibleMove
    Purpose : Updates a possible move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates a possible move
     * @param a_move a possible move
     * @param a_potentialPoints Points to be gained by a move
     * @param a_priority Priority given to a move
     * @param a_strategyCode Move strategy applied
     */
    private void updatePossibleMove(AIPossibleMoves a_move, int a_potentialPoints, int a_priority, int a_strategyCode){
        a_move.setM_potentialPoints(a_potentialPoints);
        a_move.setM_priority(a_priority);
        a_move.setM_strategyCode(a_strategyCode);
    }


    /* *********************************************************************
    Function Name : computeOrientation
    Purpose : Calculates a move orientation. (N) for north or (S) for south.
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates a move orientation.
     * @param a_rowOrigin Origin row location
     * @param a_rowDestination Destination row location
     * @return String representing the move orientation
     */
    private String computeOrientation(int a_rowOrigin, int a_rowDestination){
        if (a_rowDestination > a_rowOrigin) return "S";
        return "N";
    }


    /* *********************************************************************
    Function Name : computeFullOrientation
    Purpose : Calculates the proper orientation of a move. Northeast, Northwest, Southeast, or Southwest
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates the proper orientation of a move
     * @param a_rowOrigin Origin row location
     * @param a_colOrigin Origin row location
     * @param a_rowDestination Destination row location
     * @param a_colDestination Destination row location
     * @return String representing the proper orientation of a move
     */
    private String computeFullOrientation(int a_rowOrigin, int a_colOrigin, int a_rowDestination, int a_colDestination){
        String destination;
        if (a_rowDestination > a_rowOrigin) destination = "South";
        else destination = "North";
        if (a_colDestination > a_colOrigin) destination += "east";
        else destination += "west";

        return destination;
    }


    /* *********************************************************************
    Function Name : pickBestPossibleMove
    Purpose : Selects the best move of all the analyzed possible moves based on priority
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Selects the best move of all the analyzed possible moves based on priority
     */
    private void pickBestPossibleMove(){

        // Initialize to lowest priority and the first move
        int priority = -2;
        m_bestMove = m_possibleMoves.get(0);

        for (AIPossibleMoves move : m_possibleMoves) {
            if (move.getM_priority() > priority) {
                m_bestMove = move;
                priority = move.getM_priority();
            }
        }

        // If the highest possible move priority is 0 or less, choose randomly
        if (priority <= 0) chooseRandomMove(priority);
    }


    /* *********************************************************************
    Function Name : chooseRandomMove
    Purpose : Chooses a random move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Chooses a random move
     * @param a_priority a move priority
     */
    private void chooseRandomMove(int a_priority){
       // Build a list that contains moves equal to the priority passed
        ArrayList<AIPossibleMoves> possibleMoves = new ArrayList<AIPossibleMoves>();

        for (AIPossibleMoves move : m_possibleMoves) {
            if (move.getM_priority() == a_priority) {
                possibleMoves.add(move);
            }
        }

        // Choose a random possibleMove index
        Random r = new Random();
        int low = 0;
        int high = possibleMoves.size();
        int result = r.nextInt(high-low) + low;

        // Set the random move as the best move
        m_bestMove = possibleMoves.get(result);
    }


    /* *********************************************************************
    Function Name : makeBestPossibleMove
    Purpose : Makes the best possible move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Makes the best possible move
     * @param a_konoBoard The current kono board
     */
    private void makeBestPossibleMove(Board a_konoBoard){
        a_konoBoard.switchLocations(m_bestMove.getM_rowOrigin(),m_bestMove.getM_colOrigin(),m_bestMove.getM_rowDestination(),m_bestMove.getM_colDestination());
        SystemFeedback.setM_feedback(getStrategyDescription(m_bestMove, "cpu"));
    }

    /* *********************************************************************
    Function Name : setM_messageToUser
    Purpose : Sets a message to the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets a message to be shown to the user
     * @param a_message string representing a message to the user
     */
    private void setM_messageToUser(String a_message){
        m_messageToUser = a_message;
    }

    /* *********************************************************************
    Function Name : getM_messageToUser
    Purpose : Returns a message to the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Returns a message to be shown to the user
     * @return m_messageToUser String representing a message to the user
     */
    public final String getM_messageToUser(){ return m_messageToUser; }

    /* *********************************************************************
    Function Name : getStrategyDescription
    Purpose : Returns the strategy description
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the strategy description
     * @param a_bestMove The strategy code
     * @param a_type Type of description to calculate. It could be for the user asking for help or for the cpu describing the move made
     * @return String representing a strategy description
     */
    private String getStrategyDescription(AIPossibleMoves a_bestMove, String a_type){

        String destination = computeFullOrientation(a_bestMove.getM_rowOrigin(),a_bestMove.getM_colOrigin(),
                a_bestMove.getM_rowDestination(),a_bestMove.getM_colDestination());

        String locationOrigin = "row " + Integer.toString(a_bestMove.getM_rowOrigin()+1) + ", col "
                + Integer.toString(a_bestMove.getM_colOrigin()+1) + " ";

        String locationDestination = "row " + Integer.toString(a_bestMove.getM_rowDestination()+1) + ", col "
                + Integer.toString(a_bestMove.getM_colDestination()+1) + " ";

        String moveResult = destination + " from " + locationOrigin + "to " + locationDestination;

        if (a_type.equals("cpu")) {
            return strategyName(a_bestMove.getM_strategyCode(),"cpu") + moveResult;
        }else{
            return "Hint: Move " + moveResult + strategyName(a_bestMove.getM_strategyCode(),"user");
        }
    }


    /* *********************************************************************
    Function Name : strategyName
    Purpose : Returns a strategy name
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a strategy name
     * @param a_strategyCode A strategy code
     * @param a_type Type of strategy name to calculate
     * @return String representing a strategy name
     */
    private String strategyName(int a_strategyCode, String a_type){
        switch (a_strategyCode){
            case CAPTURE_PIECE:
                return (a_type.equals("cpu")) ? "KAI is gaining points by capturing a piece. Moved a power piece ":
                        " and capture an opponent.";
            case CAPTURE_GOAL:
                return (a_type.equals("cpu")) ? "KAI is gaining points by moving to the goal. Moved a piece ":
                        " and capture a goal location.";
            case BLOCK_OPPONENT:
                return (a_type.equals("cpu")) ? "KAI is trying to block an opponent. Moved a piece ":
                        " and block the opponent.";
            case MOVE_FORWARD:
                return (a_type.equals("cpu")) ? "KAI is moving forward towards the goal. Moved a piece ":
                        " to move towards the goal.";
            case MOVE_BACKWARD:
                return (a_type.equals("cpu")) ? "KAI is moving back to rethink strategy. Moved a piece ":
                        " move back to rethink strategy.";
        }
        return "Warning: Undefined strategy code - " + Integer.toString(a_strategyCode);
    }


    /* *********************************************************************
    Function Name : updatePieceMoved
    Purpose : Updates the board piece/location accordingly
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates the board piece/location accordingly
     * @param a_konoBoard The current kono board
     */
    private void updatePieceMoved(Board a_konoBoard){
        switch (m_bestMove.getM_strategyCode()){
            case CAPTURE_PIECE:
                a_konoBoard.setSingleContent(m_bestMove.getM_rowOrigin(),m_bestMove.getM_colOrigin(),"O");
                setM_extraPoints(5);
                break;

            case CAPTURE_GOAL:
                a_konoBoard.setSingleContent(m_bestMove.getM_rowDestination(),m_bestMove.getM_colDestination(),getM_pieceColor()+getM_pieceColor());
                break;
        }
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_possibleMoves = new ArrayList<AIPossibleMoves>();
        this.m_bestMove = new AIPossibleMoves();
        this.m_messageToUser = "";
    }
}

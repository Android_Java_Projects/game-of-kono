package com.egkono.kono;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the help layout
 It is used to display game information to the player
 **********************************************************/
public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySettings();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Override to handle option menu item chosen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            // Called when the user taps the Up button on top of the screen [<-]
            case android.R.id.home:
                backToMain();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /* *********************************************************************
    Function Name : displaySettings
    Purpose : Navigates to the Settings Activity
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Settings Activity
     */
    public void displaySettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        String message = "new_game";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    /* *********************************************************************
    Function Name : backToMain
    Purpose : Navigates to the Main Activity
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Main Activity
     */
    private void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        String message = "back_home";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}

package com.egkono.kono;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class is used to append to the app feedback
 presented to the user. Also to display messages to the user
 **********************************************************/
public class SystemFeedback {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // System feedback
    private static String m_feedback;


    /**
     * Class constructor
     */
    public SystemFeedback(){
        class_Initializer();
    }


    /* *********************************************************************
    Function Name : getM_feedback
    Purpose : Returns the system feedback
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the system feedback
     * @return String representing the system feedback to the user
     */
    public static String getM_feedback() {
        return m_feedback;
    }


    /* *********************************************************************
    Function Name : setM_feedback
    Purpose : Appends to the system feedback
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Appends to the system feedback
     * @param a_feedback Processes or steps taken by the system
     */
    public static void setM_feedback(String a_feedback) {
        // No validation required
        SystemFeedback.m_feedback = SystemFeedback.m_feedback + a_feedback + "\n";
    }


    /* *********************************************************************
    Function Name : clear_feedback
    Purpose : Clears the system feedback
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Clears the system feedback
     */
    public static void clear_feedback(){
        SystemFeedback.m_feedback = "";
    }

    /* *********************************************************************
    Function Name : snackMessageSimple
    Purpose : Display a message in a snack bar object
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays a message in a snack bar object
     * @param a_view View where the snack bar will be displayed
     * @param a_message String to display
     */
    public static void snackMessageSimple(View a_view, String a_message){
        String messageText = a_message;
        Snackbar.make(a_view, messageText, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    /* *********************************************************************
    Function Name : snackMessageAction
    Purpose : Display a message in a snack bar object with an action button
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays a message in a snack bar object with an action button
     * @param a_view View where the snack bar will be displayed
     * @param a_message String to display
     * @param a_buttonText String to display in action button
     */
    public static void snackMessageAction(View a_view, String a_message, String a_buttonText){
        final Snackbar snackbar = Snackbar.make(a_view, a_message, Snackbar.LENGTH_LONG);
        snackbar.setAction(a_buttonText, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        }).show();
    }

    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        m_feedback = "";
    }
}

package com.egkono.kono;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class holds a player's properties and functions
 **********************************************************/
public class Player {
    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Round score
    private int m_roundScore;

    // Tournament score
    private int m_tournamentScore;

    // Extra points
    private int m_extraPoints;

    // Piece color
    private String m_pieceColor;

    // Goal orientation: (N)orth or (S)outh
    private String m_goalOrientation;

    // Goal locations and scores map
    private HashMap<String,Integer> m_goalLocationsScoreUser;


    /**
     Player class constructor
     */
    public Player(){
        class_Initializer();
    }


    /* *********************************************************************
    Function Name : calculateRoundScore
    Purpose : Calculates a player's round score according to the pieces in the goal/home
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates a player's round score according to the pieces in the goal/home
     * @param a_board Current board object
     * @return int representing a player's round score
     */
    public final int calculateRoundScore(Board a_board){
        Set< Map.Entry< String,Integer> > st = m_goalLocationsScoreUser.entrySet();

        // Loop thru the map of stored goal locations and check a player's piece is in that location
        // If so, get the score value of that location and accumulate it
        String location;
        int row, col, score = 0;
        for (Map.Entry< String,Integer> me:st) {
            location = me.getKey();
            row = Integer.parseInt(location.substring(0,1));
            col = Integer.parseInt(location.substring(1,2));
            if (a_board.getSingleContent(row, col).substring(0,1).equals(m_pieceColor)) {
                score+= me.getValue();
            }
        }
        return score;
    }


    /* *********************************************************************
    Function Name : didIWin
    Purpose : Checks if the player has won the round by comparing the number of
              pieces left on the board to the number of pieces in the goal
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Checks if the player has won
     * @param a_board Current board object
     * @return True if all the pieces left are in the goal
     */
    public final boolean didIWin(Board a_board){
        return (countPiecesInGoal(a_board) == countPiecesOnBoard(a_board));
    }


    /* *********************************************************************
    Function Name : countPiecesOnBoard
    Purpose : Counts all the player's pieces left on the board
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Counts all the player's pieces left on the board
     * @param a_board Current board object
     * @return int representing the number of pieces left on the board
     */
    public final int countPiecesOnBoard(Board a_board){

        //Get the board size
        int boardSize = a_board.getM_boardSize();

        //Loop thru the board and count the pieces
        int counter = 0;
        for (int row = 0; row < boardSize; row++){
            for (int col = 0; col < boardSize; col++){
                if (a_board.getSingleContent(row, col).substring(0,1).equals(m_pieceColor)) counter++;
            }
        }
        return counter;
    }


    /* *********************************************************************
    Function Name : setM_GoalLocationsScore
    Purpose : Sets the goal locations and score per location per color
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the goal locations and score per location per color
     * @param a_boardSize Current board size
     */
    public void setM_GoalLocationsScore(int a_boardSize){

        //Goal locations and scores map
        int[] goalLine = getGoalLine(a_boardSize);

        //Set the goal row
        HashMap<String,Integer> goalLocationsScore = new HashMap<String,Integer>();
        for (int col = 0; col < a_boardSize; col++){
            String row = (m_pieceColor.equals("W") ? Integer.toString(a_boardSize - 1) : "0");
            goalLocationsScore.put(row + Integer.toString(col), goalLine[col]);

        }

        //Set the individual locations
        int row = (m_pieceColor.equals("W") ? a_boardSize - 2 : 1 );
        goalLocationsScore.put(Integer.toString(row) + "0", 1);
        goalLocationsScore.put(Integer.toString(row) + Integer.toString(a_boardSize-1), 1);

        m_goalLocationsScoreUser = goalLocationsScore;
    }


    /* *********************************************************************
    Function Name : isGoalLocation
    Purpose : Checks if the location passed is a goal/home location
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Checks if the location passed is a goal/home location
     * @param a_rowGoal Row goal location
     * @param a_colGoal Col goal location
     * @param a_goalLocationScore Goal locations and scores map
     * @return True if the location passed is a goal/home location
     */
    public final boolean isGoalLocation(int a_rowGoal, int a_colGoal, HashMap<String,Integer> a_goalLocationScore){
        int points;

        try {
            points = a_goalLocationScore.get(Integer.toString(a_rowGoal) + Integer.toString(a_colGoal) );
        }
        catch (Exception e) {
            // It is not a goal location
            return false;
        }
        return true;
    }


    /* *********************************************************************
    Function Name : getPointsInGoalLocation
    Purpose : Gets the points in a specific goal/home location
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the points in a specific goal/home location
     * @param a_rowGoal Row goal location
     * @param a_colGoal Col goal location
     * @return int representing the goal location point value
     */
    public final int getPointsInGoalLocation(int a_rowGoal, int a_colGoal, HashMap<String,Integer> a_goalLocationScore){
        return a_goalLocationScore.get(Integer.toString(a_rowGoal) + Integer.toString(a_colGoal) );
    }


    /* *********************************************************************
    Function Name : getM_goalLocationsScoreUser
    Purpose : Returns the goal locations/points map
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the goal locations/points map
     * @return HashMap representing the goal location/points
     */
    public final HashMap<String, Integer> getM_goalLocationsScoreUser(){
        return  m_goalLocationsScoreUser;
    }


    /* *********************************************************************
    Function Name : getM_pieceColor
    Purpose : Returns a player's piece color
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a player's piece color
     * @return String representing the player's piece color
     */
    public final String getM_pieceColor() {
        return m_pieceColor;
    }


    /* *********************************************************************
    Function Name : setM_pieceColor
    Purpose : Sets a player's piece color
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets a player's piece color
     * @param a_pieceColor Player's piece color
     */
    public void setM_pieceColor(String a_pieceColor) {
        if (a_pieceColor.equals("W") || a_pieceColor.equals("B")) m_pieceColor = a_pieceColor;
        else SystemFeedback.setM_feedback("Warning: A piece color can only be B or W [Player-setM_pieceColor]");
    }


    /* *********************************************************************
    Function Name : getM_roundScore
    Purpose : Returns a player's round score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a player's round score
     * @return int representing a player's round score
     */
    public final int getM_roundScore() {
        return m_roundScore;
    }


    /* *********************************************************************
    Function Name : setM_roundScore
    Purpose : Accumulates a player's round score
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Accumulates a player's round score
     * @param a_Rscore Player's round score
     */
    public void setM_roundScore(int a_Rscore) {
        if (a_Rscore >= -60) m_roundScore += a_Rscore;
        else SystemFeedback.setM_feedback("Warning: A player's round score can not be lower than -60 [Player-setM_roundScore]");
    }


    /* *********************************************************************
    Function Name : getM_tournamentScore
    Purpose : Returns a player's tournament score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a player's tournament score
     * @return int representing a player's tournament score
     */
    public final int getM_tournamentScore() {
        return m_tournamentScore;
    }


    /* *********************************************************************
    Function Name : setM_tournamentScore
    Purpose : Accumulates a player's tournament score
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Accumulates a player's tournament score
     @param a_tScore Player's tournament score
     */
    public void setM_tournamentScore(int a_tScore) {
        if (a_tScore >= 0) m_tournamentScore += a_tScore;
        else SystemFeedback.setM_feedback("Warning: A player's tournament score can not be negative [Player-setM_tournamentScore]");
    }


    /* *********************************************************************
    Function Name : getM_goalOrientation
    Purpose : Returns a player's goal orientation. (N) for north or (S) for south
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a player's goal orientation
     * @return String representing a goal orientation
     */
    public final String getM_goalOrientation() {
        return m_goalOrientation;
    }


    /* *********************************************************************
    Function Name : setM_goalOrientation
    Purpose : Sets a player's goal orientation. (N) for north or (S) for south
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets a player's goal orientation
     * @param a_goalOrientation Player's goal orientation
     */
    public void setM_goalOrientation(String a_goalOrientation) {
        if (a_goalOrientation.equals("N") || a_goalOrientation.equals("S"))  m_goalOrientation = a_goalOrientation;
        else SystemFeedback.setM_feedback("Warning: A player's goal orientation can only be N or S [Player-setM_goalOrientation]");
    }


    /* *********************************************************************
    Function Name : getM_extraPoints
    Purpose : Returns the player's extra points
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the player's extra points
     * @return int representing a player's extra points
     */
    public final int getM_extraPoints() {
        return m_extraPoints;
    }


    /* *********************************************************************
    Function Name : setM_extraPoints
    Purpose : Accumulates a player's extra points
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Accumulates a player's extra points
     * @param a_extraPoints Player's extra points
     */
    public void setM_extraPoints(int a_extraPoints) {
        if (a_extraPoints >= -5) m_extraPoints += a_extraPoints;
        else SystemFeedback.setM_feedback("Warning: A player's extra points can not be lower than -5 [Player-setM_extraPoints]");
    }


    /* *********************************************************************
    Function Name : countPiecesInGoal
    Purpose : Counts the pieces that are in the goal/home locations
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Counts the pieces that are in the goal/home locations
     * @param a_board Current board object
     * @return int representing the number of pieces in goal/home locations
     */
    private int countPiecesInGoal(Board a_board){

        Set< Map.Entry< String,Integer> > st = m_goalLocationsScoreUser.entrySet();

        // Loop thru the map of stored goal locations and check a player's piece is in that location
        String location;
        int row, col, counter = 0;
        for (Map.Entry< String,Integer> me:st) {
            location = me.getKey();
            row = Integer.parseInt(location.substring(0,1));
            col = Integer.parseInt(location.substring(1,2));
            if (a_board.getSingleContent(row, col).substring(0,1).equals(m_pieceColor)) counter++;
        }
        return counter;
    }


    /* *********************************************************************
    Function Name : getGoalLine
    Purpose : Returns the goal/home row line points according to the current board size
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Returns the goal/home row line points according to the current board size
     * @param a_boardSize Current board size
     */
    private int[] getGoalLine(int a_boardSize){
        int[] goalLineFive =  {3, 1, 5, 1, 3};
        int[] goalLineSeven = {3, 1, 5, 7, 5, 1, 3};
        int[] goalLineNine =  {3, 1, 5, 7, 9, 7, 5, 1, 3};

        switch (a_boardSize)
        {
            case 7:
                return goalLineSeven;
            case 9:
                return goalLineNine;
        }
        return goalLineFive;
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_roundScore = 0;
        this.m_tournamentScore = 0;
        this.m_extraPoints = 0;
        this.m_pieceColor = null;
        this.m_goalOrientation = null;
        this.m_goalLocationsScoreUser = null;
    }
}

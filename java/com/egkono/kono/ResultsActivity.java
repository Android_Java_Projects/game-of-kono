package com.egkono.kono;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the ResultsActivity layout
 **********************************************************/
public class ResultsActivity extends AppCompatActivity {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Kono game data
    KonoState m_konoState;


    public void reviewBoard(View view){
        m_konoState.setIsM_reviewMode(true);
        Intent intent = new Intent(this, BoardViewActivity.class);
        intent.putExtra("konoState", m_konoState);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /* *********************************************************************
    Function Name : playAnotherRound
    Purpose : Navigates to the Settings activity
              Called when the user wants to play another round
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Settings activity
     * @param view The Play Another Round button
     */
    public void playAnotherRound(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("konoState", m_konoState);
        startActivity(intent);
    }


    /* *********************************************************************
    Function Name : quitTournament
    Purpose : Displays tournament results
              Called when the user wants to quit the tournament
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays tournament results
     * @param view The Quit This Tournament button
     */
    public void quitTournament(View view){
        new AlertDialog.Builder(this)
                .setTitle("Tournament Results")
                .setMessage("Your score is: " + Integer.toString(m_konoState.getM_scoreUser()) +
                        "\nKAI's score is: " + Integer.toString(m_konoState.getM_scoreCpu()) + "\n\n" +
                        m_konoState.winLooseMessage(m_konoState.getM_scoreUser(),m_konoState.getM_scoreCpu(),"t")[0] + "\n" +
                        m_konoState.winLooseMessage(m_konoState.getM_scoreUser(),m_konoState.getM_scoreCpu(),"t")[1])
                .setNegativeButton("Keep Playing",null)
                .setPositiveButton("Back to Main", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        backToMain();
                    }
                }).create().show();
    }


    /**
     * Override given by android studio
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        // Get the kono state object passed by the calling intent
        Intent i = getIntent();
        KonoState konoState = (KonoState) i.getSerializableExtra("konoState");

        class_Initializer(konoState);
    }

    /**
     * Override to handle option menu item chosen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                reviewBoard((View)findViewById(R.id.button_review));
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Override the back button
     */
    @Override
    public void onBackPressed() {
        reviewBoard((View)findViewById(R.id.button_review));
    }

    /* *********************************************************************
    Function Name : backToMain
    Purpose : Navigates to the Main Activity
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Main Activity
     */
    private void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     * @param a_konoState Current game statistics
     */
    private void class_Initializer(KonoState a_konoState){
        // Set the kono state
        m_konoState = new KonoState();
        m_konoState = a_konoState;

        // Set the Win/Loose Message Top
        TextView wl_messageTop = (TextView) findViewById(R.id.label_result_WL_Top);
        wl_messageTop.setText(m_konoState.winLooseMessage(m_konoState.getM_scoreRoundUser(), m_konoState.getM_scoreRoundCpu(),"r")[0]);

        // Set the Win/Loose Message Bottom
        TextView wl_messageBottom = (TextView) findViewById(R.id.label_result_WL_Bottom);
        wl_messageBottom.setText(m_konoState.winLooseMessage(m_konoState.getM_scoreRoundUser(), m_konoState.getM_scoreRoundCpu(),"r")[1]);

        // Set the user's round score
        TextView userRscore = (TextView) findViewById(R.id.label_resultUserRscore);
        userRscore.setText(String.format(Locale.getDefault(), "%d",m_konoState.getM_scoreRoundUser()));

        // Set the cpu's round score
        TextView cpuRscore = (TextView) findViewById(R.id.label_resultCpuRscore);
        cpuRscore.setText(String.format(Locale.getDefault(), "%d",m_konoState.getM_scoreRoundCpu()));

        // Set the points awarded
        int pAwarded = Math.abs(m_konoState.getM_scoreRoundUser() - m_konoState.getM_scoreRoundCpu());
        TextView pointsAwarded = (TextView) findViewById(R.id.label_resultWinnerScore);
        pointsAwarded.setText(String.format(Locale.getDefault(), "%d",pAwarded));
    }
}

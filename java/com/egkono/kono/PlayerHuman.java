package com.egkono.kono;

import java.util.ArrayList;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class extends the Player class
 It contains additional properties and methods
 that only apply to the Human player
 **********************************************************/
public class PlayerHuman extends Player {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // True if a the move attempted by the user is successful
    private boolean m_moveSuccessful;

    // User click counter
    private int m_clickCounter;

    // Button locations clicked by the user
    private String m_firstButtonClickedLocation, m_secondButtonClickedLocation;

    // Board content of the locations clicked by the user
    private String m_firstContent, m_secondContent;

    // Stores all possible user moves
    private ArrayList<String> m_possibleMoves;


    /**
     * Class constructor
     */
    PlayerHuman(){
        class_Initializer();
    }


    /* *********************************************************************
    Function Name : processButtonClicked
    Purpose : Process the location clicked by the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Process the location clicked by the user
     * @param a_buttonLocation User clicked button location
     * @param a_konoBoard The current playing board
     */
    public void processButtonClicked(String a_buttonLocation, Board a_konoBoard){
        m_moveSuccessful = false;
        int row = Character.getNumericValue (a_buttonLocation.charAt(0));
        int column = Character.getNumericValue (a_buttonLocation.charAt(1));
        m_clickCounter++;
        m_possibleMoves.clear();

        if (m_clickCounter == 1){
            // Process the first piece clicked
            m_firstContent = a_konoBoard.getSingleContent(row,column);
            evaluateFirstLocationClicked(a_buttonLocation,a_konoBoard);
        }
        else if (m_clickCounter == 2){
            // Process the second piece clicked
            m_secondButtonClickedLocation = a_buttonLocation;
            m_secondContent = a_konoBoard.getSingleContent(row,column);
            evaluateSecondLocationClicked(a_buttonLocation,a_konoBoard);
        }
    }


    /* *********************************************************************
    Function Name : getM_possibleMoves
    Purpose : Returns the array of possible user moves
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the array of possible user moves
     * @return ArrayList representing all the possible moves the user could make
     */
    public final ArrayList<String> getM_possibleMoves() {
        return m_possibleMoves;
    }


    /* *********************************************************************
    Function Name : isM_moveSuccessful
    Purpose : Returns the is move successful boolean value
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the is move successful boolean value
     * @return True if the move is successful
     */
    public final boolean isM_moveSuccessful() {
        return m_moveSuccessful;
    }


    /* *********************************************************************
    Function Name : evaluateFirstLocationClicked
    Purpose : Evaluates the first board location clicked by the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Evaluates the first board location clicked by the user
     * @param a_buttonLocation User clicked button location
     * @param a_konoBoard The current playing board
     */
    private void evaluateFirstLocationClicked(String a_buttonLocation, Board a_konoBoard){
        // Check if the board location contains a user's piece
        if (m_firstContent.substring(0,1).equals(getM_pieceColor())){
            m_firstButtonClickedLocation = a_buttonLocation;
            m_possibleMoves.add(a_buttonLocation);

            // Highlight/halo possible moves the user could make
            hintPossibleMoves(a_buttonLocation, a_konoBoard);
        }
        else if (m_firstContent.equals("O")) {
            // Nothing happens, the user click on a blank space
            m_clickCounter = 0;
        }
        else{
            // User clicked on the opponent's piece
            SystemFeedback.setM_feedback("That is not your piece");
            m_clickCounter = 0;
        }
    }


    /* *********************************************************************
    Function Name : evaluateSecondLocationClicked
    Purpose : Evaluates the second board location clicked by the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Evaluates the second board location clicked by the user
     * @param a_buttonLocation User clicked button location
     * @param a_konoBoard The current playing board
     */
    private void evaluateSecondLocationClicked(String a_buttonLocation, Board a_konoBoard){
        // Check if the user clicked his/her own piece at another location
        if (m_secondContent.substring(0,1).equals(m_firstContent.substring(0,1))){
            m_firstButtonClickedLocation = a_buttonLocation;
            m_possibleMoves.add(a_buttonLocation);
            m_clickCounter = 1;

            // Hint possible moves the user could make
            hintPossibleMoves(a_buttonLocation, a_konoBoard);
        }
        else {
            //Attempt the move proposed by user
            evaluateUserMove(m_firstButtonClickedLocation,m_secondButtonClickedLocation, a_konoBoard);
            m_clickCounter = 0;
        }
    }


    /* *********************************************************************
    Function Name : hintPossibleMoves
    Purpose : Calculates all the possible moves the user can make with the first piece clicked
              Each piece only has 4 possible moves at a time (northeast, northwest, southeast, or southwest)
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates all the possible moves the user can make with the first piece clicked
     * @param a_buttonLocation User clicked button location
     * @param a_konoBoard The current playing board
     */
    private void hintPossibleMoves(String a_buttonLocation, Board a_konoBoard){
        int row = Character.getNumericValue (a_buttonLocation.charAt(0));
        int column = Character.getNumericValue (a_buttonLocation.charAt(1));

        // If any of the following movement is possible push it onto the ArrayList of m_possibleMoves
        // First possible move - northeast
        if (a_konoBoard.isMoveValid(row,column,row-1,column+1))
            m_possibleMoves.add(Integer.toString(row-1) + Integer.toString(column+1));

        // Second possible move - northwest
        if (a_konoBoard.isMoveValid(row,column,row-1,column-1))
            m_possibleMoves.add(Integer.toString(row-1) + Integer.toString(column-1));

        // Third possible move - southeast
        if (a_konoBoard.isMoveValid(row,column,row+1,column+1))
            m_possibleMoves.add(Integer.toString(row+1) + Integer.toString(column+1));

        // Fourth possible move - southwest
        if (a_konoBoard.isMoveValid(row,column,row+1,column-1))
            m_possibleMoves.add(Integer.toString(row+1) + Integer.toString(column-1));
    }


    /* *********************************************************************
    Function Name : evaluateUserMove
    Purpose : Attempts a move proposed by the user
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Attempts a move proposed by the user
     * @param a_origin Start piece location
     * @param a_destination End piece location
     * @param a_konoBoard Current kono board object
     */
    private void evaluateUserMove(String a_origin, String a_destination, Board a_konoBoard){
        int originRow = Character.getNumericValue (a_origin.charAt(0));
        int originCol = Character.getNumericValue (a_origin.charAt(1));
        int destinationRow = Character.getNumericValue (a_destination.charAt(0));
        int destinationCol = Character.getNumericValue (a_destination.charAt(1));

        if (a_konoBoard.isMoveValid(originRow,originCol,destinationRow,destinationCol)){
            m_moveSuccessful = true;
            // Make the move
            a_konoBoard.switchLocations(originRow,originCol,destinationRow,destinationCol);

            // Update piece moved
            updatePieceMoved(originRow,originCol,destinationRow,destinationCol,a_konoBoard);
        }
        else {
            m_moveSuccessful = false;
            SystemFeedback.setM_feedback(a_konoBoard.getErrorDescription());
        }
    }


    /* *********************************************************************
    Function Name : updatePieceMoved
    Purpose : Updates a board piece/location
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates a board piece/location
     * @param a_originRow Current row
     * @param a_originCol Current column
     * @param a_rowDestination Destination row
     * @param a_colDestination Destination column
     * @param a_konoBoard The kono board object
     */
    private void updatePieceMoved(int a_originRow, int a_originCol, int a_rowDestination, int a_colDestination, Board a_konoBoard){

        // Check if a piece was captured
        if ( a_konoBoard.getSingleContent(a_rowDestination,a_colDestination).equals(getM_pieceColor()+getM_pieceColor())
                && a_konoBoard.getSingleContent(a_originRow, a_originCol).substring(0,1).equals(notYourColor())){
            a_konoBoard.setSingleContent(a_originRow,a_originCol,"O");
            setM_extraPoints(5);
        }

        // Check if the player reached a goal
        if (isGoalLocation(a_rowDestination,a_colDestination,getM_goalLocationsScoreUser())) {
            a_konoBoard.setSingleContent(a_rowDestination,a_colDestination,getM_pieceColor()+getM_pieceColor());
            setM_roundScore(getPointsInGoalLocation(a_rowDestination,a_colDestination,getM_goalLocationsScoreUser()));
        }
    }


    /* *********************************************************************
    Function Name : notYourColor
    Purpose : Returns the opponents' piece color
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the opponents' piece color
     * @return String representing the opponent's piece color
     */
    private String notYourColor(){
        return (getM_pieceColor().equals("W") ? "B":"W");
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_moveSuccessful = false;
        this.m_clickCounter = 0;
        this.m_firstButtonClickedLocation = null;
        this.m_secondButtonClickedLocation = null;
        this.m_firstContent = null;
        this.m_secondContent = null;
        this.m_possibleMoves = new ArrayList<String>();
    }
}

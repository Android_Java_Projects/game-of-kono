package com.egkono.kono;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles a kono round
 **********************************************************/
public class Round {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Current Round number
    private int m_roundNumber;

    // Who the next player is
    private String m_nextPlayer;

    /**
     * Class constructor
     */
    public Round() {
        class_Initializer();
    }


    /* *********************************************************************
    Function Name : getM_roundNumber
    Purpose : Returns the round number
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the round number
     * @return int representing the round number
     */
    public final int getM_roundNumber() {
        return m_roundNumber;
    }


    /* *********************************************************************
    Function Name : setM_roundNumber
    Purpose : Sets the round number
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the round number
     @param a_roundNumber Round number
     */
    public void setM_roundNumber(int a_roundNumber) {
        if (a_roundNumber >= 0) m_roundNumber = a_roundNumber;
        else SystemFeedback.setM_feedback("Warning: A round number can not be negative [Round-setM_roundNumber]");
    }


    /* *********************************************************************
    Function Name : getM_nextPlayer
    Purpose : Returns the next player
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the next player
     * @return String representing the next player
     */
    public final String getM_nextPlayer() {
        return m_nextPlayer;
    }


    /* *********************************************************************
    Function Name : setM_nextPlayer
    Purpose : Sets the next player
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the next player
     * @param a_nextPlayer Next player
     */
    public void setM_nextPlayer(String a_nextPlayer) {
        if (a_nextPlayer.equals("Human") || a_nextPlayer.equals("Computer")) m_nextPlayer = a_nextPlayer;
        else SystemFeedback.setM_feedback("Warning: The next player can only be Human or Computer [Round-setM_nextPlayer]");
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_roundNumber = 0;
        this.m_nextPlayer = null;
    }
}

package com.egkono.kono;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Locale;
import java.util.Random;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the SettingsActivity layout
 **********************************************************/
public class SettingsActivity extends AppCompatActivity {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // A kono state
    KonoState m_konoState;

    // Who called the activity
    String m_caller;

    // Who the next player is, according to virtual dice toss up
    String m_nextPlayer;


    /**
     * Override to handle option menu item chosen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            // Called when the user taps the Up button on top of the screen [<-]
            case android.R.id.home:
                if (m_caller.equals("main")) backToMain();
                else {
                    promptUser();
                    return true;
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Override the back button
     */
    @Override
    public void onBackPressed() {
        if (m_caller.equals("main")) backToMain();
        else promptUser();
    }


    /* *********************************************************************
    Function Name : startNewRound
    Purpose : Validates user entry
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Validates user entry
     * @param view The Start a new Round button
     */
    public void startNewRound(View view) {

        //Color and board size selected
        String colorSelected = getRadioButtonText(R.id.radioGroup_color);
        String boardSizeSelected = getRadioButtonText(R.id.radioGroup_boardSize);

        if (!(colorSelected.equals("none") || boardSizeSelected.equals("none"))) {
            // Set up a kono Game
            setUpNewKonoRound(colorSelected, boardSizeSelected);
        }
        else {
            View currentView = (View)findViewById(R.id.layout_settings);
            SystemFeedback.snackMessageAction(currentView,"Please select a color and a board size","Got It!");
        }
    }

    /**
     * Override default given by android studio
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Intent i = getIntent();

        class_Initializer(i);
    }


    /* *********************************************************************
    Function Name : backToMain
    Purpose : Navigates to the Main Activity
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Main Activity
     */
    private void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        String message = "back_home";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    /* *********************************************************************
    Function Name : goToBoardView
    Purpose : Navigates to the Board View Activity
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Board View Activity
     */
    private void goToBoardView(){
        Intent intent = new Intent(this, BoardViewActivity.class);
        intent.putExtra("konoState", m_konoState);
        startActivity(intent);
    }


    /* *********************************************************************
    Function Name : setUpNewKonoRound
    Purpose : Sets up a kono state
    Parameters : See below
    Return Value : void
    Local Variables :
        kBoard, a kono game board object
        userColor, the user's piece color selected
        cpuColor, the computer's piece color selected
        konoRound, a kono game status object
    Algorithm :
        1) Instantiate a kono board
        2) Set up the player's piece colors
        3) If the caller to this activity is the main activity, set up a new kono round
        4) If the caller to this activity is the results activity, continue playing another kono round
        5) Update the kono state
        6) Go to the Board View Activity
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets up a kono state
     * @param a_colorSelected  Color selected by the user
     * @param a_boardSelected  Board size selected by the user
     */
    private void setUpNewKonoRound(String a_colorSelected, String a_boardSelected){

        // Instantiate a kono board
        Board kBoard = new Board(determineBoardSize(a_boardSelected));
        kBoard.buildNewBoard();

        // Set up the player's piece colors
        String userColor = (a_colorSelected.equals("Black") ? "B" : "W");
        String cpuColor = (a_colorSelected.equals("Black") ? "W" : "B");

        // Check who the caller is (main or result) and perform appropriate steps
        KonoState konoRound;
        if (m_caller.equals("main")){
            konoRound = new KonoState(0,0,0,0,
                    userColor,cpuColor,1,m_nextPlayer,kBoard.getM_boardSize(),kBoard.getM_Kboard(),false);
        }
        else {
            konoRound = new KonoState(m_konoState.getM_scoreUser(),m_konoState.getM_scoreCpu(),0,0,
                    userColor,cpuColor,m_konoState.getM_roundNumber() + 1,
                    m_konoState.getM_nextPlayer(),kBoard.getM_boardSize(),kBoard.getM_Kboard(),false);
        }

        // Update kono state
        m_konoState = konoRound;

        // Go to the board view activity
        goToBoardView();
    }


    /* *********************************************************************
    Function Name : getRadioButtonText
    Purpose : Get the text from the radio button selected
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Get the text from the radio button selected
     * @param a_radioButtonGroup Name of a radio button group
     * @return String representing the option text selected
     */
    private String getRadioButtonText(int a_radioButtonGroup){
        RadioGroup radioButtonGroup = (RadioGroup) findViewById(a_radioButtonGroup);

        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int radioButtonIndex = radioButtonGroup.indexOfChild(radioButton);
        RadioButton radioButtonSelected = (RadioButton)  radioButtonGroup.getChildAt(radioButtonIndex);

        String radioButtonText;
        try {
            radioButtonText = radioButtonSelected.getText().toString();
        }
        catch (Exception e) {
            radioButtonText = "none";
        }

        return radioButtonText;
    }


    /* *********************************************************************
    Function Name : determineBoardSize
    Purpose : Get the board size according to the selection made. (5X5,7X7,9X9)
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Get the board size according to the selection made
     * @param a_userChosenBoardSize Size chosen by the user
     * @return int representing the selected board size
     */
    private int determineBoardSize(String a_userChosenBoardSize){
        int boardSize = 5;
        switch (a_userChosenBoardSize) {
            case "7 x 7":
                boardSize = 7;
                break;
            case "9 x 9":
                boardSize = 9;
                break;
        }
        return boardSize;
    }


    /* *********************************************************************
    Function Name : cpuChoosesPiece
    Purpose : Computer chooses a piece color
    Parameters : See below
    Return Value : void
    Local Variables :
        rand, Random object used to choose a number between 1 and 2
        result, stores the number from rand
        rbPieceColorW, Light blue color radio button on the layout
        rbPieceColorB, Black color radio button on the layout
        userColor, user's piece color
        cpuColor, computer's piece color
        choseMessage, message to display about the piece color
        colorRGroup, radio button group containing the piece colors radio buttons
    Algorithm :
        1) Generate a random number between 1 and 2
        2) Set the piece color radio buttons
        3) Set and show the kai chose piece color message
        4) Hide the radio piece color group
    Assistance Received : none
    ********************************************************************* */
    /**
     * Computer chooses a piece color
     * Called when the cpu wins a round
     * @param a_rationale Beginning of the message to display in the piece color section.
     */
    private void cpuChoosesPiece(String a_rationale){
        // Generate a random number between 1 and 2
        Random rand = new Random();
        int result = rand.nextInt(3 - 1) + 1;

        RadioButton rbPieceColorW = (RadioButton) findViewById(R.id.radioButton_white);
        RadioButton rbPieceColorB = (RadioButton) findViewById(R.id.radioButton_black);

        // Set the piece color radio buttons
        String userColor, cpuColor;
        if (result == 1) {
            rbPieceColorW.setChecked(true);
            userColor = "Light Blue";
            cpuColor = "Black";
        }
        else {
            rbPieceColorB.setChecked(true);
            userColor = "Black";
            cpuColor = "Light Blue";
        }

        // Set and show the kai chose piece color message
        TextView choseMessage = (TextView) findViewById(R.id.settings_TV_KaiWon);
        choseMessage.setText(String.format(Locale.getDefault(), "%s",
                a_rationale + "KAI chose " + cpuColor + ". So you are " + userColor + ". KAI also gets to make the first move."));
        choseMessage.setVisibility(View.VISIBLE);

        // Hide the radio piece color group
        RadioGroup colorRGroup = (RadioGroup) findViewById(R.id.radioGroup_color);
        colorRGroup.setVisibility(View.INVISIBLE);
    }


    /* *********************************************************************
    Function Name : promptUser
    Purpose : Prompts the user about quiting the current tournament
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Prompts the user about quiting the current tournament
     */
    private void promptUser(){
        new AlertDialog.Builder(this)
                .setTitle("Tournament Results")
                .setMessage("So you decided to quit the tournament!\n\n" +
                        "Your score is: " + Integer.toString(m_konoState.getM_scoreUser()) +
                        "\nKAI's score is: " + Integer.toString(m_konoState.getM_scoreCpu()) + "\n\n" +
                        m_konoState.winLooseMessage(m_konoState.getM_scoreUser(),m_konoState.getM_scoreCpu(),"t")[0] + "\n" +
                        m_konoState.winLooseMessage(m_konoState.getM_scoreUser(),m_konoState.getM_scoreCpu(),"t")[1])
                .setNegativeButton("Keep Playing",null)
                .setPositiveButton("Continue to Main", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        backToMain();
                    }
                }).create().show();
    }


    /* *********************************************************************
    Function Name : tossUpResult
    Purpose : Performs a virtual dice toss up to determine who goes first
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Performs a virtual dice toss up to determine who goes first
     * @param a_type Type of dice set used to generate result(randomly or from a file), based on a parameter passed by MainActivity.
     */
    private void tossUpResult(String a_type){
        int userDiceThrow , cpuDiceTrow;

        do {
            userDiceThrow = tossUpGenerate(a_type);
            cpuDiceTrow = tossUpGenerate(a_type);
        } while (userDiceThrow == cpuDiceTrow);

        displayTossUpResult(userDiceThrow, cpuDiceTrow);
    }


    /* *********************************************************************
    Function Name : tossUpGenerate
    Purpose : Generates a dice throw result
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Generates a dice throw result
     * @param a_type Type of dice set used to generate result(randomly or from a file)
     * @return int representing the addition of 2 die thrown
     */
    private int tossUpGenerate(String a_type){

        // If the switch in the main activity is true, get dice results from a file
        try{
            if (a_type.equals("tossUp_fromFile")){
                FileHandler diceSetFromFile = new FileHandler();
                int resultsFromFile = diceSetFromFile.getDiceResultFromFile("DiceSetResults.txt");
                return resultsFromFile;
            }
        }catch(Exception e){
            SystemFeedback.setM_feedback("Warning: The DiceSetResults.txt could not be accessed.");
        }

        // If the switch in the main activity is false, or if DiceSetResults.txt can not be accessed,
        // choose 2 numbers randomly and add them
        Random rand = new Random();
        int dice1 = rand.nextInt(7 - 1) + 1;
        int dice2 = rand.nextInt(7 - 1) + 1;

        return dice1 + dice2;
    }


    /* *********************************************************************
    Function Name : displayTossUpResult
    Purpose : Displays virtual dice toss up results
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays virtual dice toss up results
     * @param a_userDiceResult What the user got on the virtual die thrown
     * @param a_cpuDiceResult What the computer got on the virtual die thrown
     */
    private void displayTossUpResult(int a_userDiceResult, int a_cpuDiceResult){
        String winLooseMessage;

        if (a_userDiceResult > a_cpuDiceResult) {
            winLooseMessage = "\n\nCongratulations! You win.\nYou get to choose the piece colors and play first.";
            m_nextPlayer = "Human";
        }
        else {
            winLooseMessage = "\n\nSorry, You loose.";
            cpuChoosesPiece("Since you lost the Virtual dice toss up.  ");
            m_nextPlayer = "Computer";
        }

        TextView tossUpResult = (TextView) findViewById(R.id.label_tossUp);
        tossUpResult.setText(String.format(Locale.getDefault(), "%s","Virtual dice thrown\n\nYou got: " +
                Integer.toString(a_userDiceResult) + "\nKAI got: " + Integer.toString(a_cpuDiceResult) + winLooseMessage));
        tossUpResult.setVisibility(View.VISIBLE);
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     * @param a_intent Current game statistics
     */
    private void class_Initializer(Intent a_intent){
        m_konoState = new KonoState();

        // The next player is always human
        m_nextPlayer = "Human";

        // Try to get parameter passed by MainActivity
        // If it fails, it means this activity was called by the Results Activity
        try{
            String callerParameter = a_intent.getStringExtra(EXTRA_MESSAGE);
            String[] parameterList = callerParameter.split("\\|");

            if (parameterList[0].equals("new_game")) {
                m_caller = "main";

                //tossUpResult(parameterList[1]);
            }
        }
        catch (Exception e){
            m_caller = "results";
            m_konoState = (KonoState) a_intent.getSerializableExtra("konoState");

            // Determine who chooses the piece color
            //if (!m_konoState.getM_nextPlayer().equals("Human")) cpuChoosesPiece("KAI won the last round. So he gets to choose the piece colors.  ");

        }
    }
}

package com.egkono.kono;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class is used by the Computer player
 It is used to store a possible move the computer can take
 **********************************************************/
public class AIPossibleMoves {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Origin and destination locations
    private int m_rowOrigin, m_colOrigin, m_rowDestination, m_colDestination;

    // Potential points to be gained by a move
    private int m_potentialPoints;

    // Priority of the move
    private int m_priority;

    // Strategy code to use
    private int m_strategyCode;

    // Origin and destination location contents
    private String m_contentsOrigin, m_contentsDestination;

    // Goal orientation
    private String m_goalOrientation;


    /**
     * Class constructor
     */
    public AIPossibleMoves() {
        class_Initializer();
    }


    /**
     * Class constructor with parameters
     * @param a_rowOrigin Current row location
     * @param a_colOrigin Current column location
     * @param a_rowDestination Row destination
     * @param a_colDestination Column destination
     * @param a_contentsOrigin Current location contents
     * @param a_contentsDestination Destination location contents
     * @param a_goalOrientation Player's home/goal orientation
     */
    public AIPossibleMoves(int a_rowOrigin, int a_colOrigin, int a_rowDestination, int a_colDestination,
                           String a_contentsOrigin, String a_contentsDestination, String a_goalOrientation) {
        class_Initializer();
        this.m_rowOrigin = a_rowOrigin;
        this.m_colOrigin = a_colOrigin;
        this.m_rowDestination = a_rowDestination;
        this.m_colDestination = a_colDestination;
        this.m_contentsOrigin = a_contentsOrigin;
        this.m_contentsDestination = a_contentsDestination;
        this.m_goalOrientation = a_goalOrientation;
    }


    /* *********************************************************************
    Function Name : getM_rowOrigin
    Purpose : Returns the origin board row location
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the origin board row location
     * @return int representing the origin board row location
     */
    public final int getM_rowOrigin() {
        return m_rowOrigin;
    }


    /* *********************************************************************
    Function Name : getM_colOrigin
    Purpose : Returns the origin board column location
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the origin board column location
     * @return int representing the origin board column location
     */
    public final int getM_colOrigin() {
        return m_colOrigin;
    }


    /* *********************************************************************
    Function Name : getM_rowDestination
    Purpose : Returns the destination board row location
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the destination row location
     * @return int representing the destination row board location
     */
    public final int getM_rowDestination() {
        return m_rowDestination;
    }


    /* *********************************************************************
    Function Name : getM_colDestination
    Purpose : Returns the destination board column location
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the destination col location
     * @return int representing the destination column board location
     */
    public final int getM_colDestination() {
        return m_colDestination;
    }


    /* *********************************************************************
    Function Name : getM_contentsOrigin
    Purpose : Returns the origin board location contents
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the origin board location contents
     * @return string representing the origin board location contents
     */
    public final String getM_contentsOrigin() {
        return m_contentsOrigin;
    }


    /* *********************************************************************
    Function Name : getM_contentsDestination
    Purpose : Returns the destination board location contents
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the destination contents
     * @return string representing the destination board location contents
     */
    public final String getM_contentsDestination() {
        return m_contentsDestination;
    }


    /* *********************************************************************
    Function Name : getM_goalOrientation
    Purpose : Returns the goal orientation (North or South)
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the goal orientation (North or South)
     * @return string representing the goal orientation
     */
    public final String getM_goalOrientation() {
        return m_goalOrientation;
    }


    /* *********************************************************************
    Function Name : setM_potentialPoints
    Purpose : Sets the potential number of points to be gained by this move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the potential number of points to be gained by this move
     * @param a_potentialPoints Number of points to be gained
     */
    public void setM_potentialPoints(int a_potentialPoints) {
        if (a_potentialPoints >= 0) this.m_potentialPoints = a_potentialPoints;
        else SystemFeedback.setM_feedback("Warning: The points to be gained can not be negative [AIPossibleMoves-setM_potentialPoints]");
    }


    /* *********************************************************************
    Function Name : getM_priority
    Purpose : Returns the priority of this move
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the priority of this move
     * @return int representing the priority
     */
    public final int getM_priority() {
        return m_priority;
    }


    /* *********************************************************************
    Function Name : setM_priority
    Purpose : Sets the priority of this move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the priority of this move
     * @param a_priority Priority for this move
     */
    public void setM_priority(int a_priority) {
        if (a_priority >= -2 )this.m_priority = a_priority;
        else SystemFeedback.setM_feedback("Warning: The priority can not be lower than -2 [AIPossibleMoves-setM_priority]");
    }


    /* *********************************************************************
    Function Name : getM_strategyCode
    Purpose : Returns the strategy code for this move
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the strategy code for this move
     * @return int representing the strategy code
     */
    public final int getM_strategyCode() {
        return m_strategyCode;
    }


    /* *********************************************************************
    Function Name : setM_strategyCode
    Purpose : Sets the strategy code for this move
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the strategy code of this move
     * @param a_strategyCode Strategy code for this move
     */
    public void setM_strategyCode(int a_strategyCode) {
        if (a_strategyCode >=0 )  this.m_strategyCode = a_strategyCode;
        else SystemFeedback.setM_feedback("Warning: The strategy code can not be negative [AIPossibleMoves-setM_strategyCode]");
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_rowOrigin = 0;
        this.m_colOrigin = 0;
        this.m_rowDestination = 0;
        this.m_colDestination = 0;
        this.m_potentialPoints = 0;
        this.m_priority = 0;
        this.m_strategyCode = 0;
        this.m_contentsOrigin = null;
        this.m_contentsDestination = null;
        this.m_goalOrientation = null;
    }
}

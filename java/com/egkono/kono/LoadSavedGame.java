package com.egkono.kono;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the LoadSavedGame Activity Layout
 It is used to load a game from a file
 **********************************************************/
public class LoadSavedGame extends AppCompatActivity {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // A kono state
    private KonoState m_konoState;


    /**
     * Override to handle option menu item chosen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            // Called when the user taps the Up button on top of the screen [<-]
            case android.R.id.home:
                 backToMain();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /* *********************************************************************
    Function Name : getFileContents
    Purpose : Populates file contents into a text field
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Populates file contents into a text field
     * @param view Get File Contents button
     */
    public void getFileContents(View view){

        // Objects to use
        FileHandler savedFile = new FileHandler();
        TextView editFileName = (TextView) findViewById(R.id.loadGame_fileName);
        TextView gameContents = (TextView) findViewById(R.id.loadGame_fileContents);

        // Set the contents object
        gameContents.setText(savedFile.extractGameFromFile(editFileName.getText().toString()));

        // Un-hide load contents objects
        setLoadContentsVisibility(View.VISIBLE);

        // Save the generated kono State
        m_konoState = savedFile.getM_kState();
    }


    /* *********************************************************************
    Function Name : playSavedGame
    Purpose : Navigates to the Boardview activity to start playing the saved game
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Boardview activity to start playing the saved game
     * @param view Play Saved game button
     */
    public void playSavedGame(View view){
        // Check if the Kono State is valid
        if (m_konoState != null){
            Intent intent = new Intent(this, BoardViewActivity.class);
            intent.putExtra("konoState", m_konoState);
            startActivity(intent);
        }
        else{
            Toast.makeText(getApplicationContext(), "File contents do not represent a saved Kono Game\nPlease try again", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Override called on class creation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_saved_game);

        class_Initializer();
    }


    /* *********************************************************************
    Function Name : backToMain
    Purpose : Navigates to the Main Activity
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Main Activity
     */
    private void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        String message = "back_home";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    /* *********************************************************************
    Function Name : setLoadContentsVisibility
    Purpose : Sets loaded content objects visibility
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets loaded content objects visibility
     * @param a_visible Visibility status
     */
    private void setLoadContentsVisibility(int a_visible){
        TextView gameContents = (TextView) findViewById(R.id.loadGame_fileContents);
        gameContents.setVisibility(a_visible);

        TextView labelGameContents = (TextView) findViewById(R.id.label_loadFileContents);
        labelGameContents.setVisibility(a_visible);

        Button loadPlayButton = (Button) findViewById((R.id.button_playSavedGame));
        loadPlayButton.setVisibility(a_visible);
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        m_konoState = null;

        // Hide the Game contents label, field and button
        setLoadContentsVisibility(View.INVISIBLE);

        // Sets the edit text object to the default file name
        TextView editFileName = (TextView) findViewById(R.id.loadGame_fileName);
        editFileName.setText(FileHandler.getKonoFileName());

        // Make the game contents text edit object scrollable but not editable
        TextView gameContents = (TextView) findViewById(R.id.loadGame_fileContents);
        gameContents.setKeyListener(null);
        gameContents.setFocusable(false);
        gameContents.setCursorVisible(false);
    }
}

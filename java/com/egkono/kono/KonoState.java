package com.egkono.kono;

import java.io.Serializable;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class stores a Kono State
 A Kono State stores information about the game
 **********************************************************/
public class KonoState implements Serializable {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Players data variables
    private int m_scoreUser;
    private int m_scoreCpu;
    private int m_scoreRoundUser;
    private int m_scoreRoundCpu;
    private String m_colorUser;
    private String m_colorCpu;

    // Round data variables
    private int m_roundNumber;
    private String m_nextPlayer;

    // Board data variables
    private int m_konoBoardSize;
    private String[][] m_konoBoard;
    private boolean m_reviewMode;

    /**
     * Class constructor
     */
    public KonoState(){
        class_Initializer();
    }


    /**
     * Class constructor with parameters
     * @param a_scoreUser Human's tournament score
     * @param a_scoreCpu Computer's tournament score
     * @param a_scoreRoundUser Human's round score
     * @param a_scoreRoundCpu Computer's round score
     * @param a_colorUser Human's piece color
     * @param a_colorCpu Computer's piece color
     * @param a_roundNumber Round number
     * @param a_nextPlayer Who the next player is
     * @param a_konoBoardSize The size of the kono board
     * @param a_konoBoard A 2D kono board
     */
    public KonoState(int a_scoreUser, int a_scoreCpu, int a_scoreRoundUser, int a_scoreRoundCpu, String a_colorUser, String a_colorCpu,
                     int a_roundNumber, String a_nextPlayer,int a_konoBoardSize, String[][] a_konoBoard, boolean a_reviewMode) {
        class_Initializer();
        this.m_scoreUser = a_scoreUser;
        this.m_scoreCpu = a_scoreCpu;
        this.m_scoreRoundUser = a_scoreRoundUser;
        this.m_scoreRoundCpu = a_scoreRoundCpu;
        this.m_colorUser = a_colorUser;
        this.m_colorCpu = a_colorCpu;
        this.m_roundNumber = a_roundNumber;
        this.m_nextPlayer = a_nextPlayer;
        this.m_konoBoardSize = a_konoBoardSize;
        this.m_konoBoard = a_konoBoard;
        this.m_reviewMode = a_reviewMode;
    }


    /* *********************************************************************
    Function Name : winLooseMessage
    Purpose : Returns a win, loose, or tie message
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Returns a win, loose, or tie message
     * @param a_userScore User's score
     * @param a_cpuScore Cpu's score
     * @param a_type Round message or Tournament message (r or t)
     * @return A String array representing a win or loose message
     */
    public final String[] winLooseMessage(int a_userScore, int a_cpuScore, String a_type){
        String[] messages = new String[2];

        if (a_userScore > a_cpuScore){
            messages[0] = "Congratulations!!";
            messages[1] = "YOU WIN";
        }
        else if (a_userScore < a_cpuScore) {
            messages[0] = "Sorry!";
            messages[1] = "YOU LOOSE";
        }
        else{
            messages[0] = (a_type.equals("r") ? "No points awarded to any player": "");
            messages[1] = "IT'S A TIE";
        }

        return  messages;
    }


    /* *********************************************************************
    Function Name : getM_scoreUser
    Purpose : Returns the user's tournament score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the user's tournament score
     * @return int representing the user's tournament score
    */
    public final int getM_scoreUser() {
        return m_scoreUser;
    }


    /* *********************************************************************
    Function Name : getM_scoreCpu
    Purpose : Returns the computer's tournament score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the computer's tournament score
    * @return int representing the computer's tournament score
    */
    public final int getM_scoreCpu() {
        return m_scoreCpu;
    }


    /* *********************************************************************
    Function Name : getM_scoreRoundUser
    Purpose : Returns the user's round score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the user's round score
    * @return int representing the user's round score
    */
    public final int getM_scoreRoundUser() {
        return m_scoreRoundUser;
    }


    /* *********************************************************************
    Function Name : getM_scoreRoundCpu
    Purpose : Returns the computer's round score
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the computer's round score
    * @return int representing the computer's round score
    */
    public final int getM_scoreRoundCpu() {
        return m_scoreRoundCpu;
    }


    /* *********************************************************************
    Function Name : getM_colorUser
    Purpose : Returns the user's piece color
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the user's piece color
    * @return String representing the user's piece color
    */
    public final String getM_colorUser() {
        return m_colorUser;
    }


    /* *********************************************************************
    Function Name : getM_colorCpu
    Purpose : Returns the computer's piece color
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the computer's piece color
    * @return String representing the computer's piece color
    */
    public final String getM_colorCpu() {
        return m_colorCpu;
    }


    /* *********************************************************************
    Function Name : getM_roundNumber
    Purpose : Returns the round number
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the round number
    * @return int representing the round number
    */
    public final int getM_roundNumber() {
        return m_roundNumber;
    }


    /* *********************************************************************
    Function Name : getM_nextPlayer
    Purpose : Returns the next player
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the next player
    * @return String representing the next player
    */
    public final String getM_nextPlayer() {
        return m_nextPlayer;
    }


    /* *********************************************************************
    Function Name : getM_konoBoardSize
    Purpose : Returns the kono board size
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the kono board size
    * @return int representing the board size
    */
    public final int getM_konoBoardSize() {
        return m_konoBoardSize;
    }


    /* *********************************************************************
    Function Name : getM_konoBoard
    Purpose : Returns the kono board
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
    * Gets the kono board
    * @return a String 2D array representing a kono board
    */
    public final String[][] getM_konoBoard() {
        return m_konoBoard;
    }


    /* *********************************************************************
    Function Name : getIsM_reviewMode
    Purpose : Return true if the board is being reviewed
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets if review mode is on
     * @return a boolean that specifies if the board is in review mode
     */
    public final boolean getIsM_reviewMode() {
        return m_reviewMode;
    }

 /* *********************************************************************
    Function Name : setIsM_reviewMode
    Purpose : Sets to true if board is in review mode
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets if review mode is on
     * @return a boolean that specifies if the board is in review mode
     */
    public void setIsM_reviewMode(boolean a_reviewMode) {
        m_reviewMode = a_reviewMode;
    }

    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_scoreUser = 0;
        this.m_scoreCpu = 0;
        this.m_scoreRoundUser = 0;
        this.m_scoreRoundCpu = 0;
        this.m_colorUser = null;
        this.m_colorCpu = null;

        // Round data variables
        this.m_roundNumber = 0;
        this.m_nextPlayer = null;

        // Board data variables
        this.m_konoBoardSize = 0;
        this.m_konoBoard = null;
        this.m_reviewMode = false;
    }
}

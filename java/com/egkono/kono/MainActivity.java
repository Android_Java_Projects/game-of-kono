package com.egkono.kono;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
  **********************************************************

 **********************************************************
 This class handles the MainActivity layout
 **********************************************************/
public class MainActivity extends AppCompatActivity {

    /**
     * Override default given by android studio
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    /* *********************************************************************
    Function Name : displaySettings
    Purpose : Navigates to the Settings Activity
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Settings Activity
     * @param view The Start a new tournament button
     */
    public void displaySettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        String message = "new_game";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    /* *********************************************************************
    Function Name : displayHelp
    Purpose : Navigates to the help Activity
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the load game Activity
     * @param view The How To Play Kono button
     */
    public void displayHelp(View view) {
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    /* *********************************************************************
    Function Name : rateGame
    Purpose : Opens to the play store url that contains this game
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Opens to the play store url that contains this game
     * @param view The Rate button
     */
    public void rateGame(View view) {
        String url = "https://play.google.com/store/apps/details?id=com.egkono.kono";
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    /* *********************************************************************
    Function Name : exitGame
    Purpose : Closes the game app
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Closes the game app
     * @param view The Exit button
     */
    public void exitGame(View view) {
        // Api level 15 and higher
        finishAffinity();

        // All Api levels
        finish();
    }

}

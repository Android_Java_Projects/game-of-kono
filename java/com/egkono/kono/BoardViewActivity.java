package com.egkono.kono;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the board view Activity Layout
 It is used to draw the board and statistics about the game
 **********************************************************/
public class BoardViewActivity extends AppCompatActivity {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Used to set a delay to give some time for the centerBoard method to draw the kono board properly
    private Handler myHandler;

    // Objects to use in this Activity
    Board m_konoBoard;
    PlayerHuman m_userPlayer;
    PlayerCPU m_cpuPlayer;
    Round m_konoRound;

    // Size, in pixels, of a board button
    private int m_boardButtonSize;

    // Use only when the board is being reviewed
    KonoState m_konoCurrent;

    /**
     * Override to set up the options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (m_konoBoard.getIsM_reviewMode()){
            // Review menu
            inflater.inflate(R.menu.boardviewmenureview, menu);
        }
        else {
            // Play menu
            inflater.inflate(R.menu.boardviewmenu, menu);
        }
        return true;
    }


    /**
     * Override to handle option menu item chosen
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_bv_help:
                userAskCpuForHelp();
                break;

            case R.id.menu_bv_quit:
                userQuitsRound();
                break;

            case R.id.menu_bv_play:
                playAnotherRound();
                break;

            case R.id.menu_bv_quitTour:
                quitTournament();
                break;

            case android.R.id.home:
                userQuitsRound();
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Override the back button
     */
    @Override
    public void onBackPressed() {
        if (m_konoBoard.getIsM_reviewMode()){
            doneReviewing((View)findViewById(R.id.button_review));
        }
        else userQuitsRound();
    }

    /* *********************************************************************
    Function Name : processButtonClicked
    Purpose : Processes the button/piece/location clicked by the user
              and performs appropriate checks about the game status
    Parameters : See below
    Return Value : void
    Local Variables :
        buttonPressed, the button pressed by the user
        buttonLocation, the board location where the button pressed is
    Algorithm :
        1) Check if it's the user's turn to play
        2) Get the pressed button tag, which contains the board location (row,column)
        3) Process the location in the PlayerHuman Class
        4) Update pieces/locations to halo, this hints all the possible moves the user can make
        5) Check if the attempted user move is successful
        6) Check if the user has won the round, by having all his/her pieces on the goal
           or the opponent not having more pieces left
           or if the cpu has won because its pieces lefts are home
        7) Update round, board, feedback, and highlight/halo possible moves
    Assistance Received : none
    ********************************************************************* */
    /**
     * Processes the button/piece/location clicked by the user
     * and perform appropriate checks about the game status
     * If user move is successful, the cpu can make a counter move
     * @param a_viewClicked Button object clicked by the user
     */
    public void processButtonClicked(View a_viewClicked){

        // Check if the board is in review mode
        if (isBoardInReviewMode()){ return; }

        // Check if it is the user's turn to play
        if (!m_konoRound.getM_nextPlayer().equals("Human")) {
            String message = "It is not your turn to play";
            SystemFeedback.setM_feedback(message);
            updateFeedback();
            messageInToast(message);
            return;
        }

        // Get the pressed button tag, which contains the board location (row,column)
        Button buttonPressed = (Button)a_viewClicked;
        String buttonLocation = buttonPressed.getTag().toString();

        // Process the location in the PlayerHuman Class
        m_userPlayer.processButtonClicked(buttonLocation, m_konoBoard);

        // Update pieces/locations to halo, this hints all the possible moves the user can make
        m_konoBoard.setM_highlitghtedArray(m_userPlayer.getM_possibleMoves());

        // Check if the attempted user move is successful
        if (m_userPlayer.isM_moveSuccessful()) {
            // Check if the user has won the round, by having all his/her pieces on the goal
            // or the opponent not having more pieces left
            // or if the cpu has won because its pieces lefts are home
            if (m_userPlayer.didIWin(m_konoBoard) || m_cpuPlayer.countPiecesOnBoard(m_konoBoard) == 0
                    || m_cpuPlayer.didIWin(m_konoBoard)){
                aPlayerHasWon();
            }
            else {
                // The cpu should counter move
                m_konoRound.setM_nextPlayer("Computer");
                cpuCounterMoveAfterUser();

            }
        }

        // Update round, board, feedback, and halo/hint possible moves
        updateRound();
        updateBoard();
        updateFeedback();
        updateHalosHints();
    }


    /* *********************************************************************
    Function Name : cpuConterMove
    Purpose : Makes a counter move against the human player
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Makes a counter move against the human player
     * @param view The Play Kay button
     */
    public void cpuConterMove(View view){
        try{
            m_cpuPlayer.counterMove(m_konoBoard);
            // Check if the cpu has won the round, by having all its pieces on the goal
            // or the opponent not having more pieces left
            // or if the user has won because his/her pieces lefts are home
            if (m_cpuPlayer.didIWin(m_konoBoard) || m_userPlayer.countPiecesOnBoard(m_konoBoard) == 0
                    || m_userPlayer.didIWin(m_konoBoard)){
                aPlayerHasWon();
            }
            else {
                m_konoRound.setM_nextPlayer("Human");
                //showHideKaiPlayButton(View.INVISIBLE);
            }

            // Update round, board, feedback, and halo/hint possible moves
            updateRound();
            updateBoard();
            updateFeedback();
        }
        catch (Exception e){
            noCpuMoveAvailable();
        }
    }

    /* *********************************************************************
    Function Name : cpuConterMoveAfterUser
    Purpose : Makes a counter move against the human player
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Makes a counter move against the human player
     */
    public void cpuCounterMoveAfterUser(){
        try{
            m_cpuPlayer.counterMove(m_konoBoard);
            // Check if the cpu has won the round, by having all its pieces on the goal
            // or the opponent not having more pieces left
            // or if the user has won because his/her pieces lefts are home
            if (m_cpuPlayer.didIWin(m_konoBoard) || m_userPlayer.countPiecesOnBoard(m_konoBoard) == 0
                    || m_userPlayer.didIWin(m_konoBoard)){
                aPlayerHasWon();
            }
            else {
                m_konoRound.setM_nextPlayer("Human");
            }
        }
        catch (Exception e){
            noCpuMoveAvailable();
        }
    }

    /* *********************************************************************
    Function Name : doneReviewing
    Purpose : Navigates the user to the results activity
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates the user to the results activity after reviewing board
     * @param view the button pressed
     */
    public void doneReviewing(View view){
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("konoState", m_konoCurrent);
        startActivity(intent);
    }

    /* *********************************************************************
    Function Name : quitTournament
    Purpose : Displays tournament results
              Called when the user wants to quit the tournament
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays tournament results
     */
    public void quitTournament(){
        new AlertDialog.Builder(this)
                .setTitle("Tournament Results")
                .setMessage("Your score is: " + Integer.toString(m_konoCurrent.getM_scoreUser()) +
                        "\nKAI's score is: " + Integer.toString(m_konoCurrent.getM_scoreCpu()) + "\n\n" +
                        m_konoCurrent.winLooseMessage(m_konoCurrent.getM_scoreUser(),m_konoCurrent.getM_scoreCpu(),"t")[0] + "\n" +
                        m_konoCurrent.winLooseMessage(m_konoCurrent.getM_scoreUser(),m_konoCurrent.getM_scoreCpu(),"t")[1])
                .setNegativeButton("Keep Playing",null)
                .setPositiveButton("Back to Main", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        backToMain();
                    }
                }).create().show();
    }

    /* *********************************************************************
    Function Name : playAnotherRound
    Purpose : Navigates to the Settings activity
              Called when the user wants to play another round
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Settings activity
     */
    public void playAnotherRound(){
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra("konoState", m_konoCurrent);
        startActivity(intent);
    }

    /**
     * Override default given by android studio
     */
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_view);
        myHandler = new Handler();

        // Get the kono state object passed by the calling intent
        Intent i = getIntent();
        KonoState currentkonoState = (KonoState) i.getSerializableExtra("konoState");

        // Initialize class
        class_Initializer(currentkonoState);
    }


    /**
     * Override to call the setupBoard method when this activity starts.
     * This delay is set to give the App time to do the
     * activity transition before resizing and moving Views (Board buttons).
     */
    @Override
    protected void onStart() {
        super.onStart();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setUpBoard();

                // Check if KAI won the last round, so it would play first
                if (m_konoRound.getM_nextPlayer().equals("Computer") && !m_konoBoard.getIsM_reviewMode() ) {
                    cpuCounterMoveAfterUser();
                    updateRound();
                    updateBoard();
                    updateFeedback();
                    String message = "Since KAI won the last round, it gets to play first.";
                    View currentView = (View)findViewById(R.id.layout_boardView);
                    SystemFeedback.snackMessageAction(currentView,message,"Got It!");
                }
            }
        }, 100);
    }

    /* *********************************************************************
    Function Name : aPlayerHasWon
    Purpose : Sets and calculates round statistics and navigates to the
              results activity. This function is called when the round ends
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets and calculates round statistics and navigates to the results activity
     */
    private void aPlayerHasWon(){
        Intent intent = new Intent(this, ResultsActivity.class);
        intent.putExtra("konoState", buildKonoState());
        startActivity(intent);
    }


    /* *********************************************************************
    Function Name : buildKonoState
    Purpose : Updates game statisctics and saves them into a KonoState variable when a round ends
    Parameters : none
    Return Value : See below
    Local Variables :
        userRscore, the user's round score
        cpuRscore, the computer's round score
        nextPlayer, the player who plays first on the next round
        latestKState, updated game statistics
    Algorithm :
        1) Calculate each player's round score
        2) Set player's round score
        2) Calculate who the round winner is
        3) Save current statistics
        4) Return a KonoState which contains game data
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates game statisctics and saves them into a KonoState variable when a round ends
     * @return A KonoState variable representing current game statistics
     */
    private final KonoState buildKonoState(){
        // Calculate each player's round score
        int userRscore = m_userPlayer.calculateRoundScore(m_konoBoard) + m_userPlayer.getM_extraPoints();
        int cpuRscore = m_cpuPlayer.calculateRoundScore(m_konoBoard) + m_cpuPlayer.getM_extraPoints();

        // Set player's round score
        m_userPlayer.setM_roundScore(userRscore);
        m_cpuPlayer.setM_roundScore(cpuRscore);

        // Calculate who the round winner is
        String nextPlayer;
        int roundPoints = Math.abs(userRscore - cpuRscore);
        if (userRscore > cpuRscore){
            nextPlayer = "Human";
            m_userPlayer.setM_tournamentScore(roundPoints);
        }
        else if (userRscore < cpuRscore){
            nextPlayer = "Computer";
            m_cpuPlayer.setM_tournamentScore(roundPoints);
        }
        else {
            nextPlayer = "Human";
        }
        m_konoRound.setM_nextPlayer(nextPlayer);

        // Save current statistics
        KonoState latestKState = new KonoState(m_userPlayer.getM_tournamentScore(),m_cpuPlayer.getM_tournamentScore(),
                userRscore, cpuRscore, m_userPlayer.getM_pieceColor(),
                m_cpuPlayer.getM_pieceColor(), m_konoRound.getM_roundNumber(), nextPlayer, m_konoBoard.getM_boardSize(),
                m_konoBoard.getM_Kboard(),false);

        return latestKState;
    }


    /* *********************************************************************
    Function Name : saveGame
    Purpose : Saves the current game into a kono state and then calls the function to save the game into a text file
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Saves the current game into a kono state and then calls the function to save the game into a text file
     */
    private void saveGame(){
        KonoState latestKState = new KonoState(m_userPlayer.getM_tournamentScore(),m_cpuPlayer.getM_tournamentScore(),
                0, 0, m_userPlayer.getM_pieceColor(),
                m_cpuPlayer.getM_pieceColor(), m_konoRound.getM_roundNumber(), m_konoRound.getM_nextPlayer(), m_konoBoard.getM_boardSize(),
                m_konoBoard.getM_Kboard(),false);

        FileHandler gameFile = new FileHandler(latestKState);
        gameFile.saveGameIntoFile();
        backToMain();
    }


    /* *********************************************************************
    Function Name : backToMain
    Purpose : Navigates to the Main Activity
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Navigates to the Main Activity
     */
    private void backToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        String message = "back_home";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    /* *********************************************************************
    Function Name : updateRound
    Purpose : Updates round information
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates round information
     */
    private void updateRound(){
        // Update the round number
        TextView roundNumber = (TextView) findViewById(R.id.label_roundNum);
        roundNumber.setText(String.format(Locale.getDefault(), "%d",m_konoRound.getM_roundNumber()));

        // Update score board
        TextView userRoundScore = (TextView) findViewById(R.id.label_youRscore);
        userRoundScore.setText(String.format(Locale.getDefault(), "%d", m_userPlayer.getM_extraPoints()));

        TextView userTournamentScore = (TextView) findViewById(R.id.label_youTscore);
        userTournamentScore.setText(String.format(Locale.getDefault(), "%d",m_userPlayer.getM_tournamentScore()));

        TextView cpuRoundScore = (TextView) findViewById(R.id.label_aiRscore);
        cpuRoundScore.setText(String.format(Locale.getDefault(), "%d", m_cpuPlayer.getM_extraPoints()));

        TextView cpuTournamentScore = (TextView) findViewById(R.id.label_aiTscore);
        cpuTournamentScore.setText(String.format(Locale.getDefault(), "%d",m_cpuPlayer.getM_tournamentScore()));
    }


    /* *********************************************************************
    Function Name : setUpBoard
    Purpose : Sets up a kono board
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets up a kono board
     */
    private void setUpBoard(){
        // Update the AI and user buttons
        updatePlayersButtons();

        // Initially hide all board buttons
        hideAllBoardButtons();

        // Show buttons according to the board size
        showPlayBoardButtons();

        // Center the board by moving the first row and column buttons
        centerBoard();

        // Refresh buttons according to m_konoBoard
        updateBoard();

        // Update feedback
        updateFeedback();

        // Update round information
        updateRound();

    }


    /* *********************************************************************
    Function Name : updateBoard
    Purpose : Updates the kono board buttons/locations
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates the kono board buttons/locations
     */
    private void updateBoard(){
        //Get the board and the board size
        String[][] board = new String[9][9];
        board = m_konoBoard.getM_Kboard();
        int boardSize = m_konoBoard.getM_boardSize();

        //Button location, for halo location and button type
        String buttonLocation;

        //Loop thru the board and update board location pieces
        for (int row = 0; row < boardSize; row++){
            for (int col = 0; col < boardSize; col++){
                //Get the button board location
                buttonLocation = Integer.toString(row) + Integer.toString(col);

                // Set the button's background
                setButtonBackground(buttonLocation,  board[row][col]);
            }
        }
    }


    /* *********************************************************************
    Function Name : centerBoard
    Purpose : Centers the board in the middle of the screen
    Parameters : none
    Return Value : void
    Local Variables :
        boardsize, current kono board size
        display, device display information
        size, display size
        screenWidth, device's screen width
        screenHeight, devices's screen height
        spacedTakenByButtons, the size in pixels of the space taken by a row of board buttons
        newLeftMargin, updated left margin of the first board column
        newTopMargin, updated top margin of the first board row
        buttonLocation, board button location (row and column)
    Algorithm :
        1) Get the board size
        2) Get the width and height of device's screen
        3) Calculate the number of pixels to move the first column and row board buttons
        4) For all first column and row board buttons:
           Update the left and top margins
    Assistance Received : none
    ********************************************************************* */
    /**
     * Centers the board in the middle of the screen
     */
    private void centerBoard(){
        // Get the board size
        int boardSize = m_konoBoard.getM_boardSize();

        // Get the width and height of device's screen.
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;

        // Calculate the number of pixels to move the first column and row board buttons
        int spacedTakenByButtons = boardSize * m_boardButtonSize + (boardSize - 1) * (int)convertDpToPixel(4);
        int newLeftMargin = Math.abs((screenWidth - spacedTakenByButtons)) / 2;
        int newTopMargin = (screenHeight - spacedTakenByButtons - (int)convertDpToPixel(100))/2;

        // Update the left margins of the first button column and row
        String buttonLocation;
        for (int counter = 0; counter < boardSize; counter++){
            // Move column button
            buttonLocation = Integer.toString(counter) + "0";
            changeMargin(buttonLocation,newLeftMargin,"left");

            // Move row button
            buttonLocation = "0" + Integer.toString(counter);
            changeMargin(buttonLocation,newTopMargin,"top");
        }
    }


    /* *********************************************************************
    Function Name : changeMargin
    Purpose : Updates the margins of a button
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates the margins of a button
     * @param a_buttonLocation Button location to change the margin to
     * @param a_newMargin New margin value
     * @param a_whichMargin Margin to update
     */
    private void changeMargin(String a_buttonLocation, int a_newMargin, String a_whichMargin){
        Button boardButton = (Button) findViewById(getResources().getIdentifier("button_" + a_buttonLocation, "id",
                this.getPackageName()));

        ConstraintLayout.LayoutParams lp;
        lp = (ConstraintLayout.LayoutParams)boardButton.getLayoutParams();

        int updatedLeftMargin = ((a_whichMargin.equals("left")) ? a_newMargin : lp.leftMargin);
        int updatedTopMargin = ((a_whichMargin.equals("top")) ? a_newMargin : lp.topMargin);
        int updatedRightMargin = ((a_whichMargin.equals("right")) ? a_newMargin : lp.rightMargin);
        int updatedBottomMargin = ((a_whichMargin.equals("bottom")) ? a_newMargin : lp.bottomMargin);

        lp.setMargins(updatedLeftMargin,updatedTopMargin,updatedRightMargin, updatedBottomMargin);

        // The next 2 lines are used for older android versions
        boardButton.setLayoutParams(lp);
        lp.setMargins(updatedLeftMargin,updatedTopMargin,updatedRightMargin, updatedBottomMargin);
    }


    /* *********************************************************************
    Function Name : hideAllBoardButtons
    Purpose : Hides all board buttons
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Hides all board buttons
     */
    private void hideAllBoardButtons(){
        for (int row = 0; row < 9; row++){
            for (int col = 0; col < 9; col++){
                Button boardButton = (Button) findViewById(getResources().getIdentifier("button_" + row + col, "id",
                        this.getPackageName()));
                boardButton.setVisibility(View.GONE);
            }
        }
    }


    /* *********************************************************************
    Function Name : showPlayBoardButtons
    Purpose : Displays and resizes buttons according to board size
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays and resizes buttons according to board size
     */
    private void showPlayBoardButtons(){
        // Get the board size
        int boardSize = m_konoBoard.getM_boardSize();

        // Loop thru board locations
        for (int row = 0; row < boardSize; row++){
            for (int col = 0; col < boardSize; col++){
                Button boardButton = (Button) findViewById(getResources().getIdentifier("button_" + row + col, "id",
                        this.getPackageName()));
                boardButton.setVisibility(View.VISIBLE);

                //Tag the row and column on the button
                boardButton.setTag(Integer.toString(row) + Integer.toString(col));

                // Resize the button according to the board size
                resizeButton(boardButton);
            }
        }
    }


    /* *********************************************************************
    Function Name : setM_boardButtonSize
    Purpose : Sets the board button size. Capturing the button size helps to scale the board properly
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the board button size
     */
    private void setM_boardButtonSize(){
        // Board size is 5
        m_boardButtonSize = (int) convertDpToPixel(45);
        switch (m_konoBoard.getM_boardSize()) {
            case 7:
                // Board size is 7
                m_boardButtonSize = (int) convertDpToPixel(40);
                break;

            case 9:
                // Board size is 9
                m_boardButtonSize = (int) convertDpToPixel(34);
                break;
        }
    }


    /* *********************************************************************
    Function Name : setButtonBackground
    Purpose : Sets the board button background properties
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the board button background properties
     * @param a_buttonLocation Button location
     * @param a_buttonType Type of the button (W)hite, (B)lack, Super (WW)hite, Super (BB)lack
     */
    private void setButtonBackground(String a_buttonLocation, String a_buttonType){
        Button boardButton =  (Button) findViewById(getResources().getIdentifier("button_" + a_buttonLocation , "id",
                this.getPackageName()));
        switch (a_buttonType){
            case "W":
                boardButton.setBackgroundResource(R.drawable.white_piece_button);
                break;
            case "WW":
                boardButton.setBackgroundResource(R.drawable.white_piece_queen_button);
                break;
            case "B":
                boardButton.setBackgroundResource(R.drawable.black_piece_button);
                break;
            case "BB":
                boardButton.setBackgroundResource(R.drawable.black_piece_queen_button);
                break;
            default:
                boardButton.setBackgroundResource(R.drawable.no_piece_button);
                break;
        }
    }


    /* *********************************************************************
    Function Name : resizeButton
    Purpose : Changes the size of a button
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Changes the size of a button
     * @param a_button Button to change the size to
     */
    private void resizeButton(Button a_button){
        ConstraintLayout.LayoutParams lp;
        lp = (ConstraintLayout.LayoutParams)a_button.getLayoutParams();

        lp.width = m_boardButtonSize;
        lp.height = m_boardButtonSize;

        a_button.setLayoutParams(lp);
    }


    /* *********************************************************************
    Function Name : updatePlayersButtons
    Purpose : Updates the players' button colors.
              These button are located on the of the screen, in the game statistics section
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates the players' button colors
     */
    private void updatePlayersButtons(){
        Button aiButton = (Button) findViewById(R.id.button_ai);
        Button userButton = (Button) findViewById(R.id.button_you);

        if (m_userPlayer.getM_pieceColor().equals("W")) {
            userButton.setBackgroundResource(R.drawable.white_piece_button);
            aiButton.setBackgroundResource(R.drawable.black_piece_button);
        }
        else
        {
            aiButton.setBackgroundResource(R.drawable.white_piece_button);
            userButton.setBackgroundResource(R.drawable.black_piece_button);
        }
    }


    /* *********************************************************************
    Function Name : updatePlayersButtons
    Purpose : Sets highlight/halos on board buttons
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets highlight/halos on board buttons
     */
    private void updateHalosHints(){
        ArrayList<String> buttonsToHalo = new ArrayList<String>();
        buttonsToHalo = m_konoBoard.getM_highlitghtedArray();

        if (!(buttonsToHalo.isEmpty())){
            for (String buttonLocation : buttonsToHalo) {
                int row = Character.getNumericValue (buttonLocation.charAt(0));
                int column = Character.getNumericValue (buttonLocation.charAt(1));
                haloButton(buttonLocation, m_konoBoard.getSingleContent(row,column));
            }
        }
        m_konoBoard.setM_highlitghtedArray(null);
    }


    /* *********************************************************************
    Function Name : haloButton
    Purpose : Draws a halo on a button
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     Draws a halo on a button
     @param a_buttonLocation Location of the button to highlight
     @param a_buttonType Type of button to hightlight: (W)hite, (B)lack, or empty
     */
    private void haloButton(String a_buttonLocation, String a_buttonType){
        Button boardButton = (Button) findViewById(getResources().getIdentifier("button_" + a_buttonLocation , "id",
                this.getPackageName()));

        switch (a_buttonType){
            case "W":
                boardButton.setBackgroundResource(R.drawable.white_piece_halo_button);
                break;
            case "WW":
                boardButton.setBackgroundResource(R.drawable.white_piece_queen_halo_button);
                break;
            case "B":
                boardButton.setBackgroundResource(R.drawable.black_piece_halo_button);
                break;
            case "BB":
                boardButton.setBackgroundResource(R.drawable.black_piece_queen_halo_button);
                break;
            default:
                boardButton.setBackgroundResource(R.drawable.no_piece_hint_button);
                break;
        }
    }


    /* *********************************************************************
    Function Name : convertDpToPixel
    Purpose : Calculates a value in pixels from dp
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Calculates a value in pixels from dp
     * @param a_dp a float number representing dp
     * @return float representing a value in pixels
     */
    private float convertDpToPixel(float a_dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = a_dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }


    /* *********************************************************************
    Function Name : updateFeedback
    Purpose : Updates the feedback textbox
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Updates the feedback textbox
     */
    private void updateFeedback(){
        EditText feedBack = (EditText) findViewById(R.id.editText_feedback);

        // Only print if is not empty
        if (SystemFeedback.getM_feedback() != null ){
            feedBack.setText(SystemFeedback.getM_feedback());
            feedBack.setSelection(feedBack.getText().length());

        }
    }


    /* *********************************************************************
    Function Name : userAskCpuForHelp
    Purpose : Display possible move for the user when the user asks for help
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Display possible move for the user when the user asks for help
     */
    private void userAskCpuForHelp(){
        try{
            m_cpuPlayer.userHelp(m_konoBoard,m_userPlayer.getM_pieceColor(),m_userPlayer.getM_goalOrientation(),m_userPlayer.getM_goalLocationsScoreUser());

            // Process cpu suggestion, update round, and halo/hint possible moves
            processSuggestion();
            updateBoard();
            updateHalosHints();
            updateFeedback();

            // Display a message for the user
            messageInToast(m_cpuPlayer.getM_messageToUser());

        }
        catch (Exception e){
            noHelpAvailable();
        }
    }

    /* *********************************************************************
    Function Name : messageInSnack
    Purpose : Display a message in a toast object
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Display possible move for the user when the user asks for help
     */
    private void messageInToast(String a_message){
        Toast.makeText(getApplicationContext(), a_message, Toast.LENGTH_LONG).show();
    }

    /* *********************************************************************
    Function Name : isBoardInReviewMode
    Purpose : Checks if the board is in review mode
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Display possible move for the user when the user asks for help
     */
    private boolean isBoardInReviewMode(){
        if (m_konoBoard.getIsM_reviewMode()){
            String message = "The round already ended";
            SystemFeedback.setM_feedback(message);
            updateFeedback();
            messageInToast(message);
        }
        return m_konoBoard.getIsM_reviewMode();
    }


    /* *********************************************************************
    Function Name : processSuggestion
    Purpose : Gets the move suggested by the computer
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the move suggested by the computer
     */
    private void processSuggestion(){
        ArrayList<String> possibleLocations = new ArrayList<String>();
        AIPossibleMoves userSuggetion = new AIPossibleMoves();

        // Get the best move suggested by the cpu
        userSuggetion = m_cpuPlayer.getM_bestMove();

        // Add the best move to the board's arrayList to highlight
        possibleLocations.add(Integer.toString(userSuggetion.getM_rowOrigin()) + Integer.toString(userSuggetion.getM_colOrigin()));
        possibleLocations.add(Integer.toString(userSuggetion.getM_rowDestination()) + Integer.toString(userSuggetion.getM_colDestination()));
        m_konoBoard.setM_highlitghtedArray(possibleLocations);
    }


    /* *********************************************************************
    Function Name : noHelpAvailable
    Purpose : Displays a message stating that the computer has no help available
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays a message stating that the computer has no help available
     */
    private void noHelpAvailable(){
        new AlertDialog.Builder(this)
                .setTitle("Sorry!")
                .setMessage("KAI can not think of any move to suggest. You are on your own!")
                .setPositiveButton("Back to Game",null)
                .setNegativeButton("Quit Round", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userQuitsRound();
                    }
                }).create().show();
    }


    /* *********************************************************************
    Function Name : userQuitsRound
    Purpose : Displays a message when the user wants to quit the round
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays a message when the user wants to quit the round
     */
    private void userQuitsRound(){
        new AlertDialog.Builder(this)
                .setTitle("Wait!")
                .setMessage("Are you sure you want to quit this round? You will loose 5 points if you do.")
                .setPositiveButton("Back to Game",null)
                .setNegativeButton("I am sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_userPlayer.setM_extraPoints(-5);
                        aPlayerHasWon();
                    }
                }).create().show();
    }


    /* *********************************************************************
    Function Name : noCpuMoveAvailable
    Purpose : Displays a message when the computer has no place to move
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Displays a message when the computer has no place to move
     */
    private void noCpuMoveAvailable(){
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Oops!")
                .setMessage("KAI does not have a place to move. KAI quits!")
                .setNegativeButton("Allow KAI to quit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_cpuPlayer.setM_extraPoints(-5);
                        aPlayerHasWon();
                    }
                }).create().show();
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     * @param a_konoState Current game statistics
     */
    private void class_Initializer(KonoState a_konoState){
        // Set up the board
        m_konoBoard = new Board(a_konoState.getM_konoBoardSize());
        m_konoBoard.setM_Kboard(a_konoState.getM_konoBoard().clone());
        m_konoBoard.setIsM_reviewMode(a_konoState.getIsM_reviewMode());

        // Set up the round
        m_konoRound = new Round();
        m_konoRound.setM_roundNumber(a_konoState.getM_roundNumber());
        m_konoRound.setM_nextPlayer(a_konoState.getM_nextPlayer());

        // Set up the user player
        m_userPlayer = new PlayerHuman();
        m_userPlayer.setM_pieceColor(a_konoState.getM_colorUser());
        m_userPlayer.setM_roundScore(a_konoState.getM_scoreRoundUser());
        m_userPlayer.setM_tournamentScore(a_konoState.getM_scoreUser());
        m_userPlayer.setM_GoalLocationsScore(a_konoState.getM_konoBoardSize());
        m_userPlayer.setM_goalOrientation((a_konoState.getM_colorUser().equals("W") ? "S" : "N"));

        // Set up the cpu player
        m_cpuPlayer = new PlayerCPU();
        m_cpuPlayer.setM_pieceColor(a_konoState.getM_colorCpu());
        m_cpuPlayer.setM_roundScore(a_konoState.getM_scoreRoundCpu());
        m_cpuPlayer.setM_tournamentScore(a_konoState.getM_scoreCpu());
        m_cpuPlayer.setM_GoalLocationsScore(a_konoState.getM_konoBoardSize());
        m_cpuPlayer.setM_goalOrientation((a_konoState.getM_colorCpu().equals("W") ? "S" : "N"));

        // Set/Capture the board button size, this will help resize the button accordingly
        setM_boardButtonSize();

        // Clear the feedback and make the feedback text edit object scrollable but not editable
        //SystemFeedback.clear_feedback();
        TextView feedBack = (TextView) findViewById(R.id.editText_feedback);
        feedBack.setKeyListener(null);
        feedBack.setFocusable(false);
        feedBack.setCursorVisible(false);

        // Check if is on review mode
        Button reviewButton = (Button) findViewById(R.id.button_review);
        if (a_konoState.getIsM_reviewMode()) {
            reviewButton.setVisibility(View.VISIBLE);
            m_konoCurrent = a_konoState;
        }
        else reviewButton.setVisibility(View.INVISIBLE);
    }
}

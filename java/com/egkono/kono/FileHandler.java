package com.egkono.kono;

import android.os.Environment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles file access
 It is used to save/retrieve a kono game to/from a file
 and to retrieve dice results from a file
 **********************************************************/
public class FileHandler {

    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // A kono state
    private KonoState m_kState;

    // Constants holding the default directory path and saved kono game file default name
    private static final String KONO_DIRECTORY_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GameOfKono";
    private static final String KONO_FILE_NAME = "KonoGame.txt";


    /**
     * Class constructor
     */
    public FileHandler() {
        class_Initializer();
    }


    /**
     * Class constructor with parameters
     * @param a_kState Current kono state
     */
    public FileHandler(KonoState a_kState) {
        class_Initializer();
        this.m_kState = a_kState;
    }


    /* *********************************************************************
    Function Name : saveGameIntoFile
    Purpose : Saves the game into a text file (KonoGame.txt)
    Parameters : none
    Return Value : void
    Local Variables :
        path, directory path
        dir, File object containing a directory
        file, File object containing the file path
        writer, FileWriter object used to write into the file
    Algorithm :
        1) Check if the external storage is available
        2) Get or create folder path and file
        3) Try to write to the file
    Assistance Received : none
    ********************************************************************* */
    /**
     * Saves the game into a text file
     */
    public void saveGameIntoFile(){
        // Check if the external storage is available
        SystemFeedback.setM_feedback("Saved game called");
        if (!isExternalStorageWritable()){
            SystemFeedback.setM_feedback("Error: The external storage in this device is not accessible.  The Game can not be saved.");
            return;
        }

        // Get or create folder path and file
        String path = KONO_DIRECTORY_PATH;
        File dir = new File(path);
        dir.mkdir();
        File file =  new File(path + "/" + KONO_FILE_NAME);

        // Write to the file
        try{
            FileWriter writer = new FileWriter(file);
            writer.append(buildFileBody());
            writer.flush();
            writer.close();
        }
        catch (IOException e){
            SystemFeedback.setM_feedback("Error: The save file could not be created. The Game can not be saved.");
        }
    }


    /* *********************************************************************
    Function Name : extractGameFromFile
    Purpose : Retrieves a saved kono game from a file
    Parameters : See below
    Return Value : See below
    Local Variables :
        fileContents, saved file contents
        roundNumber, the saved round number
        rangeCpu, saved computer's game data
        cpuScore, the saved computer's tournament score
        cpuColor, the saved computer's piece color
        rangeUser, saved user's game data
        userScore, the saved user's tournament score
        userColor, the saved user's piece color
        konoBoard, the saved 2D kono board
        boardSize, the size of the board
        nextPlayer, who plays next
        konoState, the saved game data
    Algorithm :
        1) Try to get the contents of the file into a String variable
        2) Parse out Round number
        3) Parse out Computer score and color
        4) Parse out User score and color
        5) Parse out Playing board
        6) Parse out the next Player
        7) Save parsed contents into the kono state member variable
        8) Return file contents
    Assistance Received : none
    ********************************************************************* */
    /**
     * Retrieves a saved kono game from a file
     * @param a_fileName Save game's file name
     * @return String representing the text content of a file
     */
    public final String extractGameFromFile(String a_fileName){
        String fileBody;

        try{
            // Get the contents of the file into a String variable
            String fileContents = readFileContents(a_fileName, false);

            // Parse out Round number
            int roundNumber = Integer.valueOf(mySubstring(fileContents,"Round:","Computer:"));

            // Parse out Computer score and color
            String rangeCpu;
            rangeCpu = mySubstring(fileContents,"Computer:","Human:");
            int cpuScore = Integer.valueOf(mySubstring(rangeCpu,"Score:","Color:"));
            String cpuColor =  mySubstring(rangeCpu,"Color:","");
            cpuColor = (cpuColor.equals("White") ? "W" : "B");

            // Parse out User score and color
            String rangeUser;
            rangeUser = mySubstring(fileContents,"Human:","Board:");
            int userScore = Integer.valueOf(mySubstring(rangeUser,"Score:","Color:"));
            String userColor =  mySubstring(rangeUser,"Color:","");
            userColor = (userColor.equals("White") ? "W" : "B");

            // Parse out Playing board
            String[][] konoBoard = constructPlayingBoard(mySubstring(fileContents,"Board:","Next Player:"));
            int boardSize = savedBoardSize(mySubstring(fileContents,"Board:","Next Player:"));

            // Parse out the next Player
            String nextPlayer = mySubstring(fileContents,"Next Player:","");

            // Save parsed contents into a kono state
            KonoState konoState = new KonoState(userScore,cpuScore,
                    0, 0, userColor,
                    cpuColor, roundNumber, nextPlayer, boardSize,
                    konoBoard.clone(),false);

            m_kState = konoState;
            fileBody = buildFileBody();
        }
        catch (Exception e){
            fileBody = "There was an error. File Contents could not be read.";
        }

        return fileBody;
    }


    /* *********************************************************************
    Function Name : getKonoFileName
    Purpose : Returns the kono default file name
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the kono default file name
     * @return String representing the default file name for a saved kono game
     */
    public static String getKonoFileName() {
        return KONO_FILE_NAME;
    }


    /* *********************************************************************
    Function Name : getM_kState
    Purpose : Returns a kono state
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a kono state
     * @return KonoState representing a kono game
     */
    public final KonoState getM_kState() {
        return m_kState;
    }


    /* *********************************************************************
    Function Name : getDiceResultFromFile
    Purpose : Gets a dice result from a randomly selected set of numbers representing 2 die thrown
              The sets of numbers are extracted from a file
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a dice result from a randomly selected set of numbers representing 2 die thrown
     * @param a_fileName The file name where sets of numbers are stored
     * @return int representing the addition of the set randomly selected
     */
    public final int getDiceResultFromFile(String a_fileName){
        String allPossibleDiceCombo = readFileContents(a_fileName,true);
        String[] allPossibleDiceSet;

        // Split by carriage return
        try {
            allPossibleDiceSet = allPossibleDiceCombo.split("\\n+");
        } catch (Exception e) {
            return 0;
        }

        // Randomly generate an index
        Random rand = new Random();
        int index = rand.nextInt(allPossibleDiceSet.length - 1) + 1;

        // Get the values of randomly selected set
        String dice1_Svalue = allPossibleDiceSet[index].substring(0,1);
        String dice2_Svalue = allPossibleDiceSet[index].substring(2,3);
        int dice1_Ivalue = Integer.valueOf(dice1_Svalue);
        int dice2_Ivalue = Integer.valueOf(dice2_Svalue);

        return dice1_Ivalue + dice2_Ivalue;
    }


    /* *********************************************************************
    Function Name : buildFileBody
    Purpose : Builds the saved game file body
    Parameters : none
    Return Value : See below
    Local Variables :
        fileBody, StringBuilder object used to concatenate data about the game
    Algorithm :
        1) Append the game data saved in a KonoState variable
        2) Return a String representing a game to save
    Assistance Received : none
    ********************************************************************* */
    /**
     * Builds the saved game file body
     * @return String representing the body of a game to save
     */
    private String buildFileBody(){
        StringBuilder fileBody = new StringBuilder();

        // Append the game data saved in the m_kState variable
        fileBody.append("Round: ");
        fileBody.append(m_kState.getM_roundNumber());
        fileBody.append("\n\n");
        fileBody.append("Computer:\n");
        fileBody.append("\tScore: ");
        fileBody.append(m_kState.getM_scoreCpu());
        fileBody.append("\n\tColor: ");
        fileBody.append((m_kState.getM_colorCpu().equals("W")) ? "White":"Black");
        fileBody.append("\n\n");
        fileBody.append("Human:\n");
        fileBody.append("\tScore: ");
        fileBody.append(m_kState.getM_scoreUser());
        fileBody.append("\n\tColor: ");
        fileBody.append((m_kState.getM_colorUser().equals("W")) ? "White":"Black");
        fileBody.append("\n\n");
        fileBody.append("Board:\n");
        String[][] konoBoard = m_kState.getM_konoBoard();
        for (int row = 0; row < m_kState.getM_konoBoardSize(); row++){
            fileBody.append("\t");
            for (int col = 0; col < m_kState.getM_konoBoardSize(); col++){
                fileBody.append(konoBoard[row][col]);
                fileBody.append(" ");
            }
            fileBody.append("\n");
        }
        fileBody.append("\nNext Player: ");
        fileBody.append(m_kState.getM_nextPlayer());

        return fileBody.toString();
    }


    /* *********************************************************************
    Function Name : readFileContents
    Purpose : Reads the contents of a file
    Parameters : See below
    Return Value : See below
    Local Variables :
        bReader, a BufferedReader object used to read every line from a file
        fReader, a FileReader object used to retrieve the file to read
        fileBody, a StringBuilder object used to concatenate file text contents
        sCurrentLine, the current line read from the file
    Algorithm :
        1) Try to retrieve and read the file read
        2) Get and append every line read
        3) Try to close the objects used to read file
        4) Return the text contents of a file
    Assistance Received : none
    ********************************************************************* */
    /**
     * Reads the contents of a file
     * @param a_fileName Name of the file to read
     * @param a_isLineSeparated True if every line read from the file is separated by a carriage return
     * @return String representing the text contents of a file
     */
    private String readFileContents(String a_fileName, boolean a_isLineSeparated){
        BufferedReader bReader = null;
        FileReader fReader = null;
        StringBuilder fileBody = new StringBuilder();

        // Try to retrieve and read the file
        try {
            fReader = new FileReader(KONO_DIRECTORY_PATH+ "/" + a_fileName);
            bReader = new BufferedReader(fReader);

            // Get and append every line read
            String sCurrentLine;
            while ((sCurrentLine = bReader.readLine()) != null) {
                fileBody.append(sCurrentLine);
                if (a_isLineSeparated) fileBody.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            fileBody.append("Error: The file can not be read");
        } finally {
            // Try to close the objects used to read file
            try {
                if (bReader != null)
                    bReader.close();
                if (fReader != null)
                    fReader.close();

            } catch (IOException ex) {
                ex.printStackTrace();
                fileBody.append("Error: The file can not be read");
            }
        }
        return fileBody.toString();
    }


    /* *********************************************************************
    Function Name : isExternalStorageWritable
    Purpose : Checks if external storage is available for read and write
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Checks if external storage is available for read and write
     * @return True if external storage is available
     */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) return true;
        return false;
    }


    /* *********************************************************************
    Function Name : mySubstring
    Purpose : Gets a string from a range of strings
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a string from a range of strings
     * @param a_wholeString The whole string
     * @param a_start Starting word
     * @param a_end Ending word
     * @return String between a_start and a_end
     */
    private String mySubstring(String a_wholeString, String a_start, String a_end){
        String result = null;

        try {
            // Get position and size of a_start
            int startPosition = a_wholeString.indexOf(a_start);
            startPosition = startPosition + a_start.length();

            // Get position of a_end if end is not blank
            int endPosition;
            if (!a_end.equals("")) endPosition =  a_wholeString.indexOf(a_end);
            else endPosition = a_wholeString.length();

            // Get the string between start and end
            result = a_wholeString.substring(startPosition,endPosition);
        } catch (Exception e) {
            e.printStackTrace();
            result = "Error: A substring could not be calculated";
        }
        return result.trim();
    }


    /* *********************************************************************
    Function Name : constructPlayingBoard
    Purpose : Builds the playing 2D board from a string
    Parameters : See below
    Return Value : See below
    Local Variables :
        oneDBoard, A String 1D array containing a kono board
        twoDBoard, A String 2D array containing a kono board
        boardSize, the size of the board
    Algorithm :
        1) Initialize a kono board
        2) Split the a_rawBoard string by white space and assign it to oneDBoard
        3) Calculate the board size
        4) For every array content in oneDBoard assign it to twoDBoard
        5) Return a 2D array representing a kono board
    Assistance Received : none
    ********************************************************************* */
    /**
     * Builds the playing 2D board from a string
     * @param a_rawBoard A Strings representing a kono board
     * @return A 2D array representing a kono board
     */
    private String[][] constructPlayingBoard(String a_rawBoard){
        String[] oneDBoard = null;
        String[][] twoDBoard = new String[9][9];

        // Initialize a kono board
        for (int row = 0; row < 9; row++){
            for (int col = 0; col < 9; col++){
                twoDBoard[row][col] = "O";
            }
        }

        // Split the a_rawBoard string by white space and assign it to oneDBoard
        try {
            oneDBoard = a_rawBoard.split("\\s+");
        } catch (Exception e) {
            return null;
        }

        // Calculate the board size
        int boardSize = (int) Math.sqrt(oneDBoard.length);
        int indexCounter = 0;

        // For every array content in oneDBoard assign it to twoDBoard
        for (int row = 0; row < boardSize; row++){
            for (int col = 0; col < boardSize; col++){
                twoDBoard[row][col] = oneDBoard[indexCounter].trim();
                indexCounter++;
            }
        }

        return twoDBoard;
    }


    /* *********************************************************************
    Function Name : savedBoardSize
    Purpose : Returns the size of the board saved in a file
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the size of the board saved in a file
     * @param a_rawBoard A string representing a kono board
     * @return int representing the size of the board
     */
    private int savedBoardSize(String a_rawBoard){
        String[] oneDBoard = null;

        // Split the raw board by white space
        try {
            oneDBoard = a_rawBoard.split("\\s+");
        } catch (Exception e) {
            return 0;
        }

        return (int) Math.sqrt(oneDBoard.length);
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_kState = null;
    }
}

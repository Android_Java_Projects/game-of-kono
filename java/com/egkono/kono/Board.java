package com.egkono.kono;

import java.util.ArrayList;

/**
 **********************************************************
 * Name:  Edgar Garcia                                    *
 * Project:  Game of Kono                                 *
 **********************************************************

 **********************************************************
 This class handles the kono board
 It is used to store methods and variables that affect the
 playing board
 **********************************************************/
public class Board {
    /*
    Order in which this class is set
    1st: Class Constants, if any
    2nd: Class Variables
    3rd: GUI Components, if any
    4th: Constructor
    5th: Event handlers, if any
    6th: Selectors
    7th: Mutators
    8th: main() method for debugging
    9th: Any utility (private) methods
    */

    // Size of the board to use
    private int m_boardSize;

    // Used to store error codes generated
    private int m_errorCode;

    // Kono board array
    private String[][] m_Kboard;

    // Used to highlight board positions
    private ArrayList<String> m_highlitghtedArray;

    // True if the board is being reviewed
    private boolean m_reviewMode;

    /**
     * Class constructor
     */
    public Board(){
        class_Initializer();
    }


     /**
      * Class constructor with parameters
      * @param a_boardSize Size of the board
      */
    public Board(int a_boardSize) {
        class_Initializer();
        this.m_boardSize = a_boardSize;
    }


    /* *********************************************************************
    Function Name : isMoveValid
    Purpose : Checks if a proposed move is valid
    Parameters : See below
    Return Value : See below
    Local Variables : none
    Algorithm :
        1) Check if the attempted move is within board bounds
        2) Check if the piece is moving diagonally
        3) Check if the current piece is a super piece and is trying to capture an opponent's piece
        4) Check if the destination is empty
        5) Return boolean which indicates if a move is valid
    Assistance Received : none
    ********************************************************************* */
    /**
     * Checks if a proposed move is valid.
     * @param a_rowOrigin Origin board row location
     * @param a_colOrigin Origin board column location
     * @param a_rowDestination Destination board row location
     * @param a_colDestination Destination board column location
     * @return True if it is a valid move
     */
    public final boolean isMoveValid(int a_rowOrigin, int a_colOrigin, int a_rowDestination, int a_colDestination){

        m_errorCode = 0;

        // Check if the attempted move is within board bounds
        if (a_rowDestination < 0 || a_rowDestination > m_boardSize - 1 || a_colDestination < 0 || a_colDestination > m_boardSize - 1) return false;

        // Check if the attempted move is moving diagonally
        if (  !((a_rowDestination == a_rowOrigin - 1 && a_colDestination == a_colOrigin + 1) ||
                (a_rowDestination == a_rowOrigin - 1 && a_colDestination == a_colOrigin - 1) ||
                (a_rowDestination == a_rowOrigin + 1 && a_colDestination == a_colOrigin + 1) ||
                (a_rowDestination == a_rowOrigin + 1 && a_colDestination == a_colOrigin - 1))
           )
        {
            m_errorCode = 1;
            return false;
        }

        // Check if the current piece is a super piece and is trying to capture an opponent's piece
        if ( (m_Kboard[a_rowOrigin][a_colOrigin].equals("WW") && m_Kboard[a_rowDestination][a_colDestination].substring(0,1).equals("B")) ||
                (m_Kboard[a_rowOrigin][a_colOrigin].equals("BB") && m_Kboard[a_rowDestination][a_colDestination].substring(0,1).equals("W")) ) {
            return true;
        }

        // Check if the destination is empty
        if ( !(m_Kboard[a_rowDestination][a_colDestination].equals("O"))) {
            m_errorCode = 2;
            return false;
        }
        return true;
    }


    /* *********************************************************************
    Function Name : switchLocations
    Purpose : Switches two board location contents
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Switches two board location contents
     * @param a_OriginRow Origin row location
     * @param a_OriginCol Origin column location
     * @param a_DestinationRow Destination row location
     * @param a_DestinationCol Destination column location
     */
    public void switchLocations(int a_OriginRow, int a_OriginCol, int a_DestinationRow, int a_DestinationCol){
        String tempHolder = m_Kboard[a_OriginRow][a_OriginCol];
        m_Kboard[a_OriginRow][a_OriginCol] = m_Kboard[a_DestinationRow][a_DestinationCol];
        m_Kboard[a_DestinationRow][a_DestinationCol] = tempHolder;
    }


    /* *********************************************************************
    Function Name : buildNewBoard
    Purpose : Builds and initializes a 2D 9 by 9 String Array
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Builds and initializes a 2D 9 by 9 String array
     */
    public void buildNewBoard(){
        // Initialize board to contain empty spaces (O)
        m_Kboard = new String[9][9];
        for (int row = 0; row < 9; row++){
            for (int col = 0; col < 9; col++){
                m_Kboard[row][col] = "O";
            }
        }

        // Set the upper and lower rows
        for (int col = 0; col < m_boardSize; col++){
            m_Kboard[0][col] = "W";
            m_Kboard[m_boardSize-1][col] = "B";
        }

        // Set the individual White pieces
        m_Kboard[1][0] = "W";
        m_Kboard[1][m_boardSize - 1] = "W";

        // Set the individual Black pieces
        m_Kboard[m_boardSize - 2][0] = "B";
        m_Kboard[m_boardSize - 2][m_boardSize - 1] = "B";
    }


    /* *********************************************************************
    Function Name : setM_Kboard
    Purpose : Sets a 2D array that represents the kono board
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets a 2D array that represents the kono board
     * @param a_konoBoard A 2D array
     */
    public void setM_Kboard(String[][] a_konoBoard){
        if (a_konoBoard != null) m_Kboard = a_konoBoard;
        else SystemFeedback.setM_feedback("Warning: The kono board to set can not be empty/null [Board-setM_Kboard]");
    }


    /* *********************************************************************
    Function Name : getM_Kboard
    Purpose : Returns the 2D array that represents the kono board
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the 2D array that represents the kono board
     * @return A 2D string array
     */
    public final String[][] getM_Kboard(){
        return m_Kboard;
    }


    /* *********************************************************************
    Function Name : getM_boardSize
    Purpose : Returns the size of the kono board
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the size of the kono board
     * @return int representing the kono board size
     */
    public final int getM_boardSize() {
        return m_boardSize;
    }


    /* *********************************************************************
    Function Name : getM_highlitghtedArray
    Purpose : Returns a String ArrayList that holds locations to highlight/halo
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets a ListArray that holds locations to highlight/halo
     * @return String ArrayList representing the locations to highlight/halo
     */
    public final ArrayList<String> getM_highlitghtedArray() {
        return m_highlitghtedArray;
    }


    /* *********************************************************************
    Function Name : setM_highlitghtedArray
    Purpose : Sets a String ArrayList that holds locations to highlight/halo
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets a String ArrayList that holds locations to highlight/halo
     * @param a_highlitghtedArray String ArrayList that holds locations to highlight/halo
     */
    public void setM_highlitghtedArray(ArrayList<String> a_highlitghtedArray) {
        // The array can be null
        this.m_highlitghtedArray = a_highlitghtedArray;
    }

    /* *********************************************************************
    Function Name : getIsM_reviewMode
    Purpose : Returns the review Mode flag
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the review Mode flag
     * @return boolean stating if the review Mode is true
     */
    public final boolean getIsM_reviewMode() { return m_reviewMode; }

    /* *********************************************************************
    Function Name : setIsM_reviewMode
    Purpose : Sets the review Mode flag
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the review Mode flag
     * @param a_reviewMode boolean representing the review mode
     */
    public void setIsM_reviewMode(boolean a_reviewMode) {
        m_reviewMode = a_reviewMode;
    }

    /* *********************************************************************
    Function Name : getSingleContent
    Purpose : Returns the content a single board location
    Parameters : See below
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the content a single board location
     * @param a_rowNumber Board row number
     * @param a_colNumber Board column number
     * @return The contents of a location
     */
    public final String getSingleContent (int a_rowNumber, int a_colNumber){
        return m_Kboard[a_rowNumber][a_colNumber];
    }


    /* *********************************************************************
    Function Name : setSingleContent
    Purpose : Sets the content a single board location
    Parameters : See below
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Sets the content on a single board location
     * @param a_rowNumber Board row number
     * @param a_colNumber Board column number
     * @param a_content Board location content
     */
    public void setSingleContent (int a_rowNumber, int a_colNumber, String a_content){
        m_Kboard[a_rowNumber][a_colNumber] = a_content;
    }


    /* *********************************************************************
    Function Name : getErrorDescription
    Purpose : Returns the last error description
    Parameters : none
    Return Value : See below
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Gets the last error description
     * @return String represent an error description
     */
    public final String getErrorDescription(){
        switch (m_errorCode){
            case 0:
                return "No error reported";
            case 1:
                return "You can only move diagonally one row/column at a time";
            case 2:
                return "You are trying to move to an invalid location";
        }
        return "Warning: " + Integer.toString(m_errorCode) + " - Error not yet defined";
    }


    /* *********************************************************************
    Function Name : class_Initializer
    Purpose : Initializes the current class's member variables
    Parameters : none
    Return Value : void
    Local Variables :
    Algorithm :
    Assistance Received : none
    ********************************************************************* */
    /**
     * Initializes the current class's member variables
     */
    private void class_Initializer(){
        this.m_boardSize = 0;
        this.m_errorCode = 0;
        this.m_Kboard = null;
        this.m_highlitghtedArray = new ArrayList<String>();
        this.m_reviewMode = false;
    }
}
